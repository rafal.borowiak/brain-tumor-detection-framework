import inspect
import re

import bt
import bt.cache
import bt.classifier
import bt.config
import bt.features
import bt.filters
import bt.images
import bt.mask
import bt.metrics
import bt.postprocessing
import bt.preprocessing
import bt.scaler
import bt.utils


def generate_doc() -> str:
    result = "\\documentclass[../thesis.tex]{subfiles}\n\\begin{document}\n\\chapter*{Dokumentacja wewnętrzna}\n\n"
    result += document_module(bt)

    modules = [(name, module) for name, module
               in inspect.getmembers(bt, predicate=inspect.ismodule)
               if has_docs(module)]
    for name, module in modules:
        result += document_module(module)
    result += "\\end{document}\n"

    result = re.sub("`((\.|,|\_|\-|=|\[|\]|\\w|\\s|\\[0-9])+)`", "\\lstinline{\\1}", result)

    return result


def has_docs(module) -> bool:
    classes = [(name, claz) for name, claz
               in inspect.getmembers(module, predicate=inspect.isclass)
               if claz.__doc__]
    functions = [(name, fn) for name, fn
                 in inspect.getmembers(module, predicate=inspect.isfunction)
                 if fn.__doc__]
    return len(classes) + len(functions) > 0


def document_classes(module) -> str:
    result = ""
    classes = [(name, claz) for name, claz
               in inspect.getmembers(module, predicate=inspect.isclass)
               if claz.__doc__]
    for name, claz in classes:
        result += document_class(claz)
    return result


def document_functions(module) -> str:
    result = ""
    functions = [(name, fn) for name, fn
                 in inspect.getmembers(module, predicate=inspect.isfunction)
                 if fn.__doc__]
    for name, fn in functions:
        correct_name = name.replace("_", "\\_")
        result += f"\\subsection*{{Funkcja {correct_name}}}\n"
        result += document_function(fn) + "\n\n"
    return result


def document_module(module) -> str:
    name = module.__name__
    result = f"\\section*{{Moduł {name}}}\n"
    result += document_classes(module)
    result += document_functions(module)
    return result


def document_class(claz) -> str:
    signature = inspect.getsource(claz).split("\n")[0].strip()[:-1]
    name = claz.__name__.replace("_", "\\_")
    doc = inspect.cleandoc(inspect.getdoc(claz))
    result = f"\\subsection*{{Klasa {name}}}\n\n"

    result += "\\begin{lstlisting}\n"
    result += f"{signature}\n"
    result += "\\end{lstlisting}\n"
    result += f"\n\\noindent {doc}\n\n"

    functions = [(name, fn) for name, fn in
                 inspect.getmembers(claz, predicate=inspect.isfunction)
                 if fn.__doc__]

    for name, fn in functions:
        correct_name = name.replace("_", "\\_")
        result += f"\\subsubsection{{Metoda {correct_name}}}\n"
        result += document_function(fn) + "\n\n"

    return result


def document_function(function) -> str:
    source = inspect.getsource(function).split("\n")
    signature = source[0].strip()
    if not signature.strip().endswith(":"):
        for line in source[1:]:
            if line.strip().endswith(":"):
                signature += "\n" + line.strip()
                break
            elif line.strip().startswith("def "):
                signature += "\n" + line.strip()
            else:
                signature += "\n\t" + line.strip()
    signature = signature[:-1]
    doc_lines = inspect.cleandoc(inspect.getdoc(function)).split("\n")
    args = []
    returns = ""
    for index, line in enumerate(doc_lines):
        if line.startswith(":param "):
            args += [line.replace(":param ", "")]
            doc_lines[index] = ""
        if line.startswith(":return: "):
            for second_index, second_line in enumerate(doc_lines[index:]):
                returns += second_line.replace(":return: ", "") + " "
                doc_lines[second_index + index] = ""

    result = "\\begin{lstlisting}\n"
    result += f"{signature}\n"
    result += "\\end{lstlisting}\n"

    result += "\\noindent " + "\n".join(doc_lines).strip()
    result += "\n"
    result += "\\newline\n\n"

    if len(args) > 0:
        result += "\\noindent \\textbf{Argumenty}\n\\begin{itemize}\n"
        for arg in args:
            split = arg.split(" ")
            arg_name = split[0][:-1]
            arg_doc = " ".join(split[1:])
            result += f"\\item \\lstinline{{{arg_name}}} -- {arg_doc},\n"
        result = result[:-2] + ".\n"
        result += "\\end{itemize}\n"

    if len(returns) > 0:
        result += "\n"
        result += "\\noindent \\textbf{Wartość zwracana}\n\n"
        result += "\\noindent " + returns

    return result


if __name__ == "__main__":
    print(generate_doc())
