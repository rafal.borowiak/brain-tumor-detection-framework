from bt.metrics.image_quality_measures import ImageQualityMeasures
import numpy as np
import SimpleITK as sitk


def test_uiq_should_be_1_for_same_images():
    image = sitk.GetImageFromArray(np.random.randint(0, 256, size=(100, 100, 100)))

    measures = ImageQualityMeasures(image, image)

    assert measures.universal_image_quality_index == 1.0


def test_uiq_should_be_negative_1_for_given_images():
    array = np.random.uniform(-1.0, 1.0, size=(100, 100, 100))
    opposite_array = - array + 2 * array.mean()

    image = sitk.GetImageFromArray(array)
    opposite_image = sitk.GetImageFromArray(opposite_array)

    measures = ImageQualityMeasures(image, opposite_image)

    assert measures.universal_image_quality_index == -1.0

