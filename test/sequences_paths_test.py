import os
import tempfile

import pytest

from bt.images import paths_utils


@pytest.fixture(scope="module")
def test_files():
    files = [tempfile.gettempdir() + "/" + it for it in ["bt_file1", "bt_file2", "bt_file3"]]

    for file in files:
        open(file, 'a').close()

    yield files

    for file in files:
        os.remove(file)


def test_resolving_paths_on_normal_paths():
    paths = ["file1", "file2", "file3"]

    assert paths == paths_utils.resolve_globs(paths)


def test_resolving_paths_with_star(test_files):
    paths = [tempfile.gettempdir() + "/bt_*"]

    assert paths_utils.resolve_globs(paths) == test_files


def test_resolving_paths_with_question_mark(test_files):
    paths = [tempfile.gettempdir() + "/bt_file?"]

    assert paths_utils.resolve_globs(paths) == test_files


def test_resolving_paths_with_range(test_files):
    paths = [tempfile.gettempdir() + "/bt_file[1-3]"]

    assert paths_utils.resolve_globs(paths) == test_files


def test_resolving_paths_mixsed(test_files):
    paths = [tempfile.gettempdir() + "/bt_file*", "other_file"]

    assert paths_utils.resolve_globs(paths) == test_files + ["other_file"]
