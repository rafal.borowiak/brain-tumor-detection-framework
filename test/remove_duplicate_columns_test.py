import numpy as np
from bt.features import RemoveDuplicateColumns

def test_fit_transform_remove_duplicate_columns_fiting_and_transforming():
    test_array = np.array(
        [
            [0, 0, 4, 0, 2, 43],
            [1, 2, 8, 5, 4, 54],
            [3, 6, 6, 15, 3, 54]
        ]
    )

    expected_array = np.array(
        [
            [0, 4, 43],
            [1, 8, 54],
            [3, 6, 54]
        ]
    )

    selector = RemoveDuplicateColumns()

    assert np.array_equal(selector.fit_transform(test_array), expected_array)
