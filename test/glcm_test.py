from bt.features import calculate_glcm, glcm_features
import numpy as np


def test_should_calculate_correct_glcm_in_vertical_direction():
    array = np.array(
        [
            [
                [0, 1],
                [0, 1]
            ],
            [
                [2, 1],
                [1, 2]
            ]
        ]
    )

    glcm = calculate_glcm(array, max=2, direction=(0, 1, 0))
    expected_glcm = np.array(
        [
            [2, 0, 0],
            [0, 2, 2],
            [0, 2, 0]
        ]
    )

    assert glcm.shape == (3, 3)
    assert np.array_equal(glcm, expected_glcm)


def test_should_calculate_correct_glcm_in_negative_direction():
    array = np.array(
        [
            [
                [0, 1],
                [0, 1]
            ],
            [
                [2, 1],
                [1, 2]
            ]
        ]
    )

    glcm = calculate_glcm(array, max=2, direction=(0, -1, 0))
    expected_glcm = np.array(
        [
            [2, 0, 0],
            [0, 2, 2],
            [0, 2, 0]
        ]
    )

    assert glcm.shape == (3, 3)
    assert np.array_equal(glcm, expected_glcm)


def test_should_calculate_correct_glcm_in_horizontal_direction():
    array = np.array(
        [
            [
                [0, 1],
                [0, 1]
            ],
            [
                [2, 1],
                [1, 2]
            ]
        ]
    )

    glcm = calculate_glcm(array, max=2, direction=(0, 0, 1))
    expected_glcm = np.array(
        [
            [0, 2, 0],
            [2, 0, 2],
            [0, 2, 0]
        ]
    )

    assert glcm.shape == (3, 3)
    assert np.array_equal(glcm, expected_glcm)


def test_should_calculate_correct_glcm_in_depth_direction():
    array = np.array(
        [
            [
                [0, 1],
                [0, 1]
            ],
            [
                [2, 1],
                [1, 2]
            ]
        ]
    )

    glcm = calculate_glcm(array, max=2, direction=(1, 0, 0))
    expected_glcm = np.array(
        [
            [0, 1, 1],
            [1, 2, 1],
            [1, 1, 0]
        ]
    )

    assert glcm.shape == (3, 3)
    assert np.array_equal(glcm, expected_glcm)


def test_should_calculate_correct_glcm_in_diagonal_direction():
    array = np.array(
        [
            [
                [0, 1],
                [0, 1]
            ],
            [
                [2, 1],
                [1, 2]
            ]
        ]
    )

    glcm = calculate_glcm(array, max=2, direction=(1, 1, 1))
    expected_glcm = np.array(
        [
            [0, 0, 1],
            [0, 0, 0],
            [1, 0, 0]
        ]
    )

    assert glcm.shape == (3, 3)
    assert np.array_equal(glcm, expected_glcm)
