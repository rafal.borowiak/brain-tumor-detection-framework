from bt.features.glcm import glcm_features_fast, glcm_features, calculate_glcm
import numpy


def test_full_sized_features():
    image = numpy.random.randint(0, 100, (3, 3, 3))

    glcm = calculate_glcm(image, max=99, direction=(0, 0, 1))

    expected = glcm_features(glcm)
    actual = glcm_features_fast(glcm, 0, 99)

    numpy.testing.assert_allclose(actual, expected)


def test_optimized_features():
    image = numpy.random.randint(25, 76, (3, 3, 3))

    glcm = calculate_glcm(image, max=99, direction=(0, 0, 1))

    expected = glcm_features(glcm)
    actual = glcm_features_fast(glcm, int(image.min()), int(image.max()))

    numpy.testing.assert_allclose(actual, expected)
