import SimpleITK as sitk
import numpy as np

from bt.mask.mask_iterator import IterableMask


def test_iterable_mask_should_iterate_over_nonzero_elements():
    mask_array = np.array([
        [
            [0, 1],
            [1, 0]
        ],
        [
            [0, 0],
            [1, 1]
        ]
    ])
    mask = sitk.GetImageFromArray(mask_array)

    indices = list(IterableMask(mask))

    expected = {(1, 0, 0), (0, 1, 0), (0, 1, 1), (1, 1, 1)}

    assert len(indices) == len(set(indices))
    assert set(indices) == expected
