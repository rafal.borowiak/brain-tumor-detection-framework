from bt.utils.numeric_dict_reduce import *


def test_common_keys_should_return_dicts_common_keys():
    dicts = [
        {"a":1,"b":2,"c":3,"d":4},
        {"b":2,"c":3,"d":4},
        {"a":1,"b":2,"c":3}
    ]

    assert {"b", "c"} == common_keys(dicts)


def test_dicts_concat_should_concat_dicts_values():
    dicts = [
        {"a":1,"b":{"c":"str","d":[1, 1]}},
        {"a": 2, "b": {"c": "str", "d": [2, 2]}},
        {"a": 3, "b": {"c": "str", "d": [3, 3]}}
    ]

    assert {"a": [1, 2, 3], "b": {"c": ["str", "str", "str"], "d": [[1, 1], [2, 2], [3, 3]]}} == dict_concat(dicts)


def test_dicts_mean():
    dicts = [
        {"a":1,"b":{"c":"str","d":[1, 1]}},
        {"a": 2, "b": {"c": "str", "d": [2, 2]}},
        {"a": 3, "b": {"c": "str", "d": [3, 3]}}
    ]

    assert {"a": 2, "b": {"c": ["str", "str", "str"], "d": [2, 2]}} == dicts_numpy_stat(dicts, np.mean)


def test_dicts_max():
    dicts = [
        {"a":1,"b":{"c":"str","d":[1, 1]}},
        {"a": 2, "b": {"c": "str", "d": [2, 3]}},
        {"a": 3, "b": {"c": "str", "d": [3, 2]}}
    ]

    assert {"a": 3, "b": {"c": ["str", "str", "str"], "d": [3, 3]}} == dicts_numpy_stat(dicts, np.max)

def test_dicts_min():
    dicts = [
        {"a":1,"b":{"c":"str","d":[1, 1]}},
        {"a": 2, "b": {"c": "str", "d": [0, 2]}},
        {"a": 3, "b": {"c": "str", "d": [3, 3]}}
    ]

    assert {"a": 1, "b": {"c": ["str", "str", "str"], "d": [0, 1]}} == dicts_numpy_stat(dicts, np.min)


