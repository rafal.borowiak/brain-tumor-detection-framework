import logging
import os
from pathlib import Path

from jsonobject import *
import SimpleITK as sitk

from bt.utils.path import only_filename
from .mask import create_mask, Mask
from bt.preprocessing import PreprocessingConfiguration


class MaskConfiguration(JsonObject):
    load_if_present = BooleanProperty(default=True)
    create_if_absent = BooleanProperty(default=True)
    name_suffix = StringProperty(default="_mask")
    path = StringProperty()
    preprocessing = ObjectProperty(PreprocessingConfiguration)

    def mask_path(self, paths: [str]):
        name = "_".join(map(lambda p: only_filename(p), paths))
        name += self.name_suffix + ".mha"
        return self.path + "/" + name

    def read_or_create(self, paths: [str], images: [sitk.Image]) -> Mask:
        logger = logging.getLogger(__name__)
        mask_preprocessing = self.preprocessing.create_preprocessing_chain()
        mask_path = self.mask_path(paths)

        if self.load_if_present and Path(mask_path).is_file():
            logger.info(f"Reading mask from file: {mask_path}...")
            mask = sitk.ReadImage(mask_path)
        else:
            logger.info(f"Creating mask...")
            mask = create_mask(images)
            if self.create_if_absent:
                logger.info(f"Saving mask: {mask_path}...")
                sitk.WriteImage(sitk.Cast(mask, sitk.sitkUInt16), mask_path, True)

        mask = mask_preprocessing(mask)
        return Mask(mask)
