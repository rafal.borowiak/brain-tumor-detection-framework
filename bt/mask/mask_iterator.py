import SimpleITK as sitk
import numpy as np


class MaskIterator:
    def __init__(self, np_array):
        self.iterator = np.ndenumerate(np_array)

    def __next__(self):
        value = False

        while not value:
            indices, value = self.iterator.next()

        return indices[::-1]


class IterableMask:
    def __init__(self, mask: sitk.Image):
        self.mask_array = sitk.GetArrayFromImage(mask)

    def __iter__(self) -> MaskIterator:
        return MaskIterator(self.mask_array)


def nonzero_indices(mask: sitk.Image):
    return list(IterableMask(mask))
