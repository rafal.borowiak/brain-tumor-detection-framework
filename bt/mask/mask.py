import random
from typing import List, Tuple

import SimpleITK as sitk
from functional import seq
from lazy import lazy
import numpy
import functools

from .mask_iterator import nonzero_indices


def create_mask(images: List[sitk.Image]) -> sitk.Image:
    """
    Funkcja tworząca maskę na podstawie przekazanego zestawu obrazów.

    :param images: lista maskowanych obrazów
    :return: Zwraca binarny obraz maski.
    """
    arrays = seq(images) \
        .map(lambda i: sitk.GetArrayFromImage(sitk.BinaryErode(sitk.BinaryMorphologicalClosing(i > 0, 5), 5))) \
        .to_list()

    result = functools.reduce(lambda x, y: numpy.logical_and(x, y), arrays)

    return sitk.GetImageFromArray(result.astype(int))


class Mask:
    """
    Klasa zawierająca obraz maski i metody operujące na nim.
    """
    def __init__(self, image: sitk.Image):
        """
        Konstruktor przyjmujący obraz maski.

        :param image: binarny obraz maski
        """
        self.image = image

    @lazy
    def indices(self):
        return nonzero_indices(self.image)

    def __iter__(self):
        return iter(self.indices)

    def __len__(self) -> int:
        return len(self.indices)

    def __getitem__(self, index: int) -> Tuple[int, int, int]:
        return self.indices[index]

    def __sub__(self, other: 'Mask') -> 'Mask':
        this_array = sitk.GetArrayFromImage(self.image)
        other_array = sitk.GetArrayFromImage(other.image)
        result_array = ((this_array - other_array) > 0).astype(int)
        result = sitk.GetImageFromArray(result_array)
        return Mask(result)

    def __eq__(self, other: 'Mask') -> bool:
        """
        Operator porównania masek.

        :param other: maska, prawa strona operatora porównania
        :return: Zwraca `True` jeśli maski są identyczne, `False` w przeciwnym razie.
        """
        return isinstance(other, Mask) and numpy.array_equal(
                sitk.GetImageFromArray(self.image),
                sitk.GetArrayFromImage(other.image)
            )
