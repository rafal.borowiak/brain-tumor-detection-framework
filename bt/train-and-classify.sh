#!/bin/bash

export PYTHONPATH=.
python3 train.py ../config.yaml
python3 classify.py ../classification.yaml
