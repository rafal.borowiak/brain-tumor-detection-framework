import logging

import SimpleITK as sitk

import yaml

from bt.classifier.classifier_facade import ClassifierFacade
from bt.config.classification_configuration import ClassificationConfiguration
from bt.config.config_file_utils import configure_loggers, configs_paths, config_files
from bt.features import Classes
from bt.metrics.metrics import Metrics
from bt.utils.numeric_dict_reduce import dicts_numpy_stat
from bt.utils.path import only_filename
import numpy


def classify(config):
    all_metrics = []
    classifier = ClassifierFacade(config)
    truth_images = config.truth_images.load_images()
    for truth, (sequence, classified_classes) in zip(truth_images, classifier):
        try:
            path = config.result_path + "/" + only_filename(list(sequence.paths.values())[0]) + f"_result.mha"
            logger.info(f"Saving result to {path}...")
            sitk.WriteImage(classified_classes.image, path)

            truth_classes = Classes(truth, sequence.mask, number_of_classes=5)

            logger.info("Calculating metrics...")
            metrics = Metrics(
                title=only_filename(list(sequence.paths.values())[0]),
                actual=truth_classes,
                predicted=classified_classes
            )

            scores_text = yaml.dump(metrics.as_dict())

            all_metrics += [metrics.as_dict()]

            if config.report_file_path:
                with open(config.report_file_path, "w") as f:
                    f.write(yaml.dump(all_metrics))

            if config.stats_file_path:
                with open(config.stats_file_path, "w") as f:
                    stats = {
                        "mean": dicts_numpy_stat(all_metrics, numpy.mean),
                        "min": dicts_numpy_stat(all_metrics, numpy.min),
                        "max": dicts_numpy_stat(all_metrics, numpy.max),
                    }
                    f.write(yaml.dump(stats))

            print(scores_text)
        except Exception as e:
            logger.error(e)


def detect_duplication(mapper, configs, name):
    logger = logging.getLogger(__name__)
    models = set(map(mapper, configs))

    if len(models) != len(configs):
        logger.fatal(f"{name} is duplicated somewhere!")
        exit(1)


if __name__ == "__main__":
    configure_loggers()
    logger = logging.getLogger(__name__)

    config_files_paths = configs_paths()
    logger.info(f"Config paths: {config_files_paths}")

    config_files_list = config_files()
    configs = list(map(ClassificationConfiguration, config_files_list))

    detect_duplication(lambda x: x.model, configs, "Model path")
    detect_duplication(lambda x: x.report_file_path, configs, "Report file path")
    detect_duplication(lambda x: x.stats_file_path, configs, "Stats file path")

    for path, config in zip(config_files_paths, configs):
        try:
            logger.info(f"Using config: {path}")
            classify(config)
        except Exception as e:
            logger.exception(f"Classification failed! {e}")
