import logging

from bt.config.training_configuration import TrainingConfiguration
from bt.features.tissue import StageClasses
from bt.classifier.multistage_classifier import MultistageClassifier


class Trainer:
    """
    Klasa fasady umożliwiającej przeprowadzenie procesu uczenia na podstawie konfiguracji.
    """
    def __init__(self, config: TrainingConfiguration):
        """
        Konstruktor inicjalizujący pola klasy.

        :param config: konfiguracja procesu uczenia
        """
        self.config = config
        self.logger = logging.getLogger(__name__)

        self.logger.info(f"Using config: {config.to_json()}")

    def train(self) -> MultistageClassifier:
        """
        Przeprowadza pełny proces uczenia na podstawie przekazanej w konstruktorze konfiguracji.

        :return: Zwraca obiekt zawierający nauczone klasyfikatory.
        """
        config = self.config
        first_stage = config.first_stage.create_classifier_scaler()
        edema = config.edema.create_classifier_scaler()
        core = config.core.create_classifier_scaler()

        sequences = config.load_classified_patient_sequences()
        references = config.reference_images

        features_constructors = config.create_features_constructors()

        features_classes = sequences.random_features_and_classes(features_constructors)

        first_stage_features, first_stage_classes = features_classes[StageClasses.FirstStage]
        first_stage_classes = first_stage_classes > 0

        self.logger.info("Performing first stage training...")
        first_stage.fit(first_stage_features, first_stage_classes)

        edema_features, edema_classes = features_classes[StageClasses.Edema]

        self.logger.info("Performing edema training...")
        edema.fit(edema_features, edema_classes)

        core_features, core_classes = features_classes[StageClasses.Core]

        self.logger.info("Performing core training...")
        core.fit(core_features, core_classes)

        return MultistageClassifier(
            first_stage=first_stage,
            edema=edema,
            core=core,
            references=references,
            config=self.config
        )
