import logging

from bt.config.training_configuration import TrainingConfiguration
from bt.config.config_file_utils import configure_loggers, config_files, configs_paths
from bt.trainer import Trainer


def train(config):
    model = Trainer(config).train()
    model.save()


if __name__ == "__main__":
    configure_loggers()
    logger = logging.getLogger(__name__)

    config_files_paths = configs_paths()
    logger.info(f"Config paths: {config_files_paths}")

    config_files_list = config_files()
    configs = list(map(TrainingConfiguration, config_files_list))

    models = set(map(lambda x: x.model_output, configs))

    if len(models) != len(configs):
        logger.fatal("Model output path is duplicated somewhere!")
        exit(1)

    for path, config in zip(config_files_paths, configs):
        try:
            logger.info(f"Starting training: {path}")
            model = Trainer(config).train()
            model.save()
        except Exception as e:
            logger.exception(f"Training failed! {e}")
