import numpy as np

from numba import jit
from typing import List


@jit(nopython=True, nogil=True)
def calculate_glcm(array: np.ndarray, max: int, direction: (int, int, int)) -> np.ndarray:
    glcm = np.zeros((max + 1, max + 1), dtype=np.int32)
    end = [0, 0, 0]
    begin = [0, 0, 0]

    for i in range(3):
        if direction[i] >= 0:
            end[i] = array.shape[i] - direction[i]
        else:
            begin[i] = -direction[i]
            end[i] = array.shape[i]

    for x in range(begin[0], end[0]):
        for y in range(begin[1], end[1]):
            for z in range(begin[2], end[2]):
                x_end = x + direction[0]
                y_end = y + direction[1]
                z_end = z + direction[2]

                i = array[x, y, z]
                j = array[x_end, y_end, z_end]

                glcm[i, j] += 1
                glcm[j, i] += 1

    return glcm

@jit(nopython=True, nogil=True)
def glcm_features_fast(glcm: np.ndarray, min_graylevel: int, max_graylevel: int):
    i_array = np.array(list(range(min_graylevel, max_graylevel + 1)))
    glcm = glcm[min_graylevel:(max_graylevel+1), min_graylevel:(max_graylevel+1)]
    glcm = glcm / glcm.sum()

    u = (i_array * glcm).sum()
    variance = ((i_array - u) ** 2 * glcm).sum()

    energy = 0
    entropy = 0
    contrast = 0
    idm = 0
    correlation = 0.0

    for i in i_array:
        for j in i_array:
            g = glcm[i - min_graylevel, j - min_graylevel]
            indices_difference_square = (i - j)*2

            energy += g**2
            entropy -= 0 if g == 0 else (np.log(g) * g)
            contrast += indices_difference_square * g
            idm += g / (1 + indices_difference_square)
            if variance != 0.0:
                correlation += (i - variance) * (j - variance) * g

    if variance != 0.0:
        correlation = correlation / variance
    else:
        correlation = 1.0

    return [
        float(energy),  # A
        float(entropy),  # B
        float(contrast),  # C
        float(variance),  # D
        float(correlation),  # E
        float(idm)  # F
    ]


@jit(nopython=True, nogil=True)
def glcm_features(glcm: np.ndarray):
    glcm = glcm / glcm.sum()
    indices_difference_square = np.zeros(glcm.shape)

    for i in range(indices_difference_square.shape[0]):
        for j in range(indices_difference_square.shape[1]):
            indices_difference_square[i, j] = (i - j)*2

    energy = np.multiply(glcm, glcm).sum()

    entropy = -np.multiply(np.where(glcm > 0, np.log(glcm), np.zeros(glcm.shape)), glcm).sum()

    contrast = np.multiply(indices_difference_square, glcm).sum()

    inverse_difference_moment = np.multiply(1 / (1 + indices_difference_square), glcm).sum()

    i_array = np.array(list(range(glcm.shape[0])))

    u = (i_array * glcm).sum()
    variance = ((i_array - u) ** 2 * glcm).sum()

    correlation = 0.0

    if variance != 0.0:
        for i in i_array:
            for j in i_array:
                correlation += (i - variance) * (j - variance) * glcm[i, j]

        correlation = correlation / variance
    else:
        correlation = 1.0

    return [
        float(energy),  # A
        float(entropy),  # B
        float(contrast),  # C
        float(variance),  # D
        float(correlation),  # E
        float(inverse_difference_moment)  # F
    ]


@jit(nopython=True, nogil=True)
def calculate_glcm_features(array: np.array, max: int) -> np.ndarray:
    directions = [
        (1, 0, 0), (0, 1, 0), (0, 0, 1)
    ]

    features = [0.0]
    features.pop()

    for direction in directions:
        glcm = calculate_glcm(array=array, max=max, direction=direction)
        features = features + glcm_features_fast(glcm, array.min(), array.max())

    return features
