from lazy import lazy

from bt.images.image_type import ImageType

from typing import Dict, List, Tuple

import SimpleITK as sitk
from functional import seq

from .features import Features
from bt.mask import Mask
from .feature_header import FeatureHeader


class SymmetryFeatures(Features):
    """
    Klasa reprezentująca cechy symetrii.
    """
    def __init__(
            self,
            images: Dict[ImageType, sitk.Image],
            mask: Mask
    ):
        """
        Konstruktor inicjalizujący pola klasy.

        :param images: słownik typów obrazów i obrazów
        :param mask: obszar maski, w którym wyznaczane są wektory cech
        """
        self.images = {image_type: sitk.DiscreteGaussian(image, variance=3.0) for image_type, image in images.items()}
        self.mask = mask
        self._size = next(iter(images.values())).GetSize()

    @lazy
    def _mask_center(self):
        min, max = (self.size[0], 0)
        for index in self.mask.indices:
            if index[0] < min:
                min = index[0]
            if index[0] > max:
                max = index[0]
        return (max + min) // 2

    def header(self) -> List['FeatureHeader']:
        image_types = self.images.keys()

        return seq(image_types) \
            .map(lambda type: [FeatureHeader(type, "symmetry", "value"), FeatureHeader(type, "symmetry", "difference")]) \
            .flatten() \
            .to_list()

    def __getitem__(self, coordinates):
        x = coordinates[0]
        distance = abs(x - self._mask_center)
        mirror_x = (self._mask_center + distance) if x < self._mask_center else (self._mask_center - distance)
        mirror_coordinates = (mirror_x, coordinates[1], coordinates[2])

        return seq(self.images.values()) \
            .map(lambda image: [image[coordinates], image[coordinates] - image[mirror_coordinates]]) \
            .flatten() \
            .to_list()

    @property
    def size(self) -> Tuple[int]:
        return self._size