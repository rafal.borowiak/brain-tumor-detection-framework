from typing import Dict, List, Tuple

import SimpleITK as sitk
from functional import seq

from .features_calculator import FeaturesCalculator
from .features import Features
from fn.iters import map
from bt.mask import Mask
from .feature_header import FeatureHeader


class WindowedFeatures(Features):
    def __init__(
            self,
            images: Dict['ImageType', sitk.Image],
            mask: Mask,
            features_calculator: FeaturesCalculator,
            name: str,
            window_size: int = 3
    ):
        self.images = images
        self.mask = mask
        self._size = next(iter(images.values())).GetSize()
        self.window_size = window_size
        self.half_ws = window_size // 2
        self.features_calculator = features_calculator
        self.name = name

    @property
    def size(self) -> int:
        return self._size

    def header(self) -> List[FeatureHeader]:
        names = self.features_calculator.names()
        features_per_image = len(names)

        image_types = seq(self.images.keys()) \
                .map(lambda image_type: [image_type] * features_per_image) \
                .flatten() \
                .to_list()

        return seq(zip(image_types, names * len(self.images))) \
                .map(lambda type_name: FeatureHeader(type_name[0], self.name, type_name[1])) \
                .to_list()

    def _upper_coordinates(self, coordinates) -> Tuple[int, int, int]:
        return tuple(map(lambda x: x + self.half_ws + 1, coordinates))

    def _lower_coordinates(self, coordinates) -> Tuple[int, int, int]:
        return tuple(map(lambda x: max(x - self.half_ws, 0), coordinates))

    def coordinates_slices(self, coordinates) -> Tuple[slice, slice, slice]:
        (lower_x, lower_y, lower_z) = self._lower_coordinates(coordinates)
        (upper_x, upper_y, upper_z) = self._upper_coordinates(coordinates)
        return slice(lower_x, upper_x, 1), slice(lower_y, upper_y, 1), slice(lower_z, upper_z, 1)
