from typing import Callable

from functional import seq
from jsonobject import *

from bt.features.symmetry_features_configuration import SymmetryFeaturesConfiguration
from bt.features.transformer_configuration import TransformerConfiguration
from .textural_features_configuration import TexturalFeaturesConfiguration
from .features import FeaturesComposite
from .statistical_features_configuration import StatisticalFeaturesConfiguration
from .histogram_configuration import HistogramConfiguration
from sklearn.pipeline import make_pipeline

_type_map = {
    "first_order": StatisticalFeaturesConfiguration,
    "statistical": StatisticalFeaturesConfiguration,
    "histogram": HistogramConfiguration,
    "second_order": TexturalFeaturesConfiguration,
    "textural": TexturalFeaturesConfiguration,
    "symmetry": SymmetryFeaturesConfiguration
}


class FeaturesConfiguration(JsonObject):
    features = ListProperty(DictProperty())
    transformers = ListProperty(TransformerConfiguration)

    def create_features_constructor(self) -> Callable:
        features = seq(self.features) \
            .map(lambda it: _type_map[it["type"]](it)) \
            .filter(lambda it: it) \
            .map(lambda it: it.create_features_constructor())

        return lambda i, m: FeaturesComposite(
            features_list=features.map(lambda it: it(i, m)).to_list(),
            config=self.features
        )

    def create_preprocessing_pipeline(self):
        pipeline = seq(self.transformers) \
            .map(lambda it: it.create_transformer()) \
            .to_list()

        return make_pipeline(*pipeline)
