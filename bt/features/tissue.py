from aenum import IntEnum


class Tissue(IntEnum):
    """
    Typ wyliczeniowy określający typ tkanki.

    \\noindent Wartości statyczne:
    \\begin{itemize}
        \item `Tissue.NORMAL` -- reprezentuje zdrową tkankę i tło,
        \item `Tissue.NECROTIC_CORE` -- reprezentuje martwicę,
        \item `Tissue.EDEMA` -- reprezentuje obrzęk,
        \item `Tissue.NONENHANCING_TUMOR` -- reprezentuje guz niewzmacniający się kontrastowo,
        \item `Tissue.ENHANCING_TUMOR` -- reprezentuje guz wzmacniający się kontrastowo.
    \end{itemize}
    """
    NORMAL = 0
    NECROTIC_CORE = 1
    EDEMA = 2
    NONENHANCING_TUMOR = 3
    ENHANCING_TUMOR = 4


class TumorRegion:
    """
    Klasa zawierająca jedynie pola statyczne określające regiony guza -- cały guz, rdzeń guza i aktywny
    obszar guza.

    \\noindent Wartości statyczne:
    \\begin{itemize}
        \item `TumorRegion.Whole` -- zbiór tkanek całego obszaru guza,
        \item `TumorRegion.Core` -- zbiór tkanek wewnątrz rdzenia guza,
        \item `TumorRegion.Active` -- zbiór jednoelementowy, zawierający guz wzmacniajacy się kontrastowo.
    \end{itemize}
    """
    Whole = frozenset({Tissue.NECROTIC_CORE, Tissue.EDEMA, Tissue.ENHANCING_TUMOR, Tissue.NONENHANCING_TUMOR})
    Core = frozenset({Tissue.NECROTIC_CORE, Tissue.ENHANCING_TUMOR, Tissue.NONENHANCING_TUMOR})
    Active = frozenset({Tissue.ENHANCING_TUMOR})


class StageClasses:
    """
    Klasa zawierająca jedynie pola statyczne określające regiony guza dla
    danego etapu segmentacji.

    \\noindent Wartości statyczne:
    \\begin{itemize}
        \item `StageClasses.FirstStage` -- pierwszy etap segmentacji, rozróżniania jest tkanka normalna
        od rdzenia guza,
        \item `StageClasses.Core` -- etap segmentacji, w którym rozróżniane są tkanki wewnątrz guza,
        \item `StageClasses.Edema` -- etap segmentacji, w którym rożróżniania jest tkanka zdrowa i obrzęk.
    \end{itemize}
    """
    FirstStage = frozenset({Tissue.NORMAL, TumorRegion.Core})
    Edema = frozenset({Tissue.NORMAL, Tissue.EDEMA})
    Core = TumorRegion.Core
