from jsonobject import *
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.preprocessing import StandardScaler, MinMaxScaler, RobustScaler, MaxAbsScaler
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.feature_selection import SelectKBest, SelectPercentile, GenericUnivariateSelect, VarianceThreshold

from bt.features.remove_duplicate_columns import RemoveDuplicateColumns

_allowed_transformers = [t.__name__ for t in [
    StandardScaler,
    MinMaxScaler,
    RobustScaler,
    MaxAbsScaler,
    PCA,
    LinearDiscriminantAnalysis,
    QuadraticDiscriminantAnalysis,
    SelectKBest,
    SelectPercentile,
    GenericUnivariateSelect,
    VarianceThreshold,
    RemoveDuplicateColumns
]]


class TransformerConfiguration(JsonObject):
    """
    Konfiguracja procesu przekształcania cech. Umożliwia między innymi ustawienie standaryzacji
    cech czy redukcji wymiarowości.
    """
    type = StringProperty()
    parameters = DictProperty(default={})

    def _camel_case_type(self):
        words = self.type.replace('-', ' ').replace('_', ' ').split(' ')

        scaler_type = ''.join(x[0].upper() + x[1:] for x in words)

        return scaler_type

    def create_transformer(self):
        scaler_type = self._camel_case_type()

        if scaler_type not in _allowed_transformers:
            raise AttributeError(
                f"{self.type} is not allowed transformer type! Translation: {scaler_type}."
                f" Use one of: {_allowed_transformers}"
            )

        return eval(scaler_type)(**self.parameters)
