import collections
from typing import Union, Iterable, List, Tuple

from functional import seq
from lazy import lazy

from .feature_header import FeatureHeader
from bt.filters import binarize
from .features import Features
from bt.mask import Mask
import SimpleITK as sitk
from tqdm import tqdm
import random


class Classes(Features):
    """
    Obiekt reprezentujący klasy przypisane wokselom obrazu.
    """
    def __init__(self, truth_image: sitk.Image, mask: Mask, number_of_classes: int):
        """
        Konstruktur inicjalizujący pola klasy.

        :param truth_image: obraz odniesienia, przyjmujący maksymalnie `number_of_classes-1` poziomów szarości.
        :param mask: obraz maski
        :param number_of_classes: liczba klas
        """
        self.image = truth_image
        self.mask = mask
        self.number_of_classes = number_of_classes
        self.labels = list(range(number_of_classes))

    def __getitem__(self, coordinates):
        return self.image[coordinates]

    @property
    def size(self) -> int:
        return self.image.GetSize()

    def header(self) -> List['FeatureHeader']:
        return [FeatureHeader(None, "classes", "class")]

    @lazy
    def class_indices(self) -> List[List[Tuple[int, int, int]]]:
        """
        Metoda generująca listę podwieszaną ze współrzędnymi wokseli.
        Długość listy jest równa liczbie klas w obrazie odniesienia. Każda z list
        podwiesznych zawiera współrzędne wokseli o klasie wyznaczonej przez indeks list.

        :return: Zwraca listę podwieszaną ze współrzędnymi wokseli nalężacych do danej klasy.
        """
        indicies = [[] for i in self.labels]
        for i in tqdm(self.mask, desc="Sampling"):
            indicies[self.image[i]] += [i]
        return indicies

    def random_coordinates_of_class(self, size: int, classes: Union[int, Iterable[int]]) -> List[Tuple[int, int, int]]:
        """
        Metoda generująca losową próbkę współrzędnych wokseli należących do klas wskazanych parametrem `classes`.

        :param size: rozmiar wygenerowanej próbki
        :param classes: wartość klasy lub lista wartości klas
        :return: Zwraca listę zawierającą losowo wygenerowaną próbkę współrzędnych wokseli należących do odpowiedniej klasy.
        """
        class_indices = seq(classes) \
            .filter(lambda clas: clas < self.number_of_classes) \
            .flat_map(lambda clas: self.class_indices[clas]).to_list()
        if len(class_indices) == 0:
            return []
        return random.sample(class_indices, k=min(size, len(class_indices)-1))

    def class_vector(self) -> List[int]:
        """
        :return: Zwraca listę klas dla wokseli dla obszaru maski.
        """
        return self.select(self.mask.indices)

    def map_as_positive(self, classes: List[int]) -> 'Classes':
        """
        Konstruuje obiekt `Classes`, w którym klasy przekazane w argumencie `classes` są zamienione na wartość 1, a
        pozostałe na wartość 0.

        :param classes: lista wartości klasy, które mają być potraktowane jako pozytywne
        :return: Zwraca nowy, zbinaryzowany obiekt `Classes`.
        """
        return Classes(binarize(self.image, classes), self.mask, number_of_classes=2)
