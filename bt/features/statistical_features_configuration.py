from typing import Dict

from bt.features.windowed_numpy_array_features import WindowedNumpyArrayFeatures
from bt.images.image_type import ImageType
from .statistical_features import StatisticalFeaturesCalculator
from .windowed_features_configuration import WindowedFeaturesConfiguration
import SimpleITK as sitk


class StatisticalFeaturesConfiguration(WindowedFeaturesConfiguration):
    """
    Konfiguracja cech statystycznych.
    """
    def _features_constructor(self, images: Dict[ImageType, sitk.Image], mask: 'Mask'):
        return WindowedNumpyArrayFeatures(images, mask, StatisticalFeaturesCalculator(), "first_order", self.window_size)
