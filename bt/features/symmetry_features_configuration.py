from typing import Dict

from bt.features.base_features_configuration import BaseFeaturesConfiguration
from bt.images.image_type import ImageType
from .symmetry_features import SymmetryFeatures
import SimpleITK as sitk


class SymmetryFeaturesConfiguration(BaseFeaturesConfiguration):
    """
    Konfiguracja cech symetrii.
    """
    def _features_constructor(self, images: Dict[ImageType, sitk.Image], mask: 'Mask'):
        return SymmetryFeatures(images, mask)
