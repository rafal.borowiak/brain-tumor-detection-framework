from typing import Iterable, Set

import numpy

class FeaturesClasses:
    def __init__(self, features: numpy.ndarray, classes: numpy.ndarray):
        self.features = features
        self.classes = classes

    def __getitem__(self, item: int):
        return [self.features, self.classes][item]

    def __len__(self):
        return 2

    def filter_by_classes(self, allowed_classes: Set[int]) -> 'FeaturesClasses':
        filtered = [(feature, clas) for feature, clas in zip(self.features, self.classes) if clas in allowed_classes]
        if len(filtered) > 0:
            features, classes = zip(*filtered)
            return FeaturesClasses(numpy.array(features), numpy.array(classes))
        else:
            return FeaturesClasses(numpy.array([]), numpy.array([]))
