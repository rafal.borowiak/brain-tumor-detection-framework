from jsonobject import *

from bt.features.base_features_configuration import BaseFeaturesConfiguration


class WindowedFeaturesConfiguration(BaseFeaturesConfiguration):
    window_size = IntegerProperty()
