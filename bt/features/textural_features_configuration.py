from bt.features.windowed_features_configuration import WindowedFeaturesConfiguration
from bt.images.image_type import ImageType
from .textural_features import TexturalFeatures
import SimpleITK as sitk
from typing import Dict


class TexturalFeaturesConfiguration(WindowedFeaturesConfiguration):
    """
    Konfiguracja cech teskturalnych.
    """
    def _features_constructor(self, images: Dict[ImageType, sitk.Image], mask: 'Mask'):
        return TexturalFeatures(images, mask, self.window_size)
