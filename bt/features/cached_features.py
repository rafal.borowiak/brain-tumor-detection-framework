import logging
from typing import Dict, List, Tuple, Iterable

import diskcache
from lazy import lazy
from tqdm import tqdm

from .features import Features
from bt.mask import Mask
from bt.images.image_type import ImageType
import SimpleITK as sitk
import numpy
import mmh3


class Key:
    def __init__(self, images: Dict[ImageType, sitk.Image], mask: Mask, key_prefix: str, coordinates: List[Tuple[int]]):
        self.images = {key:sitk.GetArrayFromImage(image) for key, image in images.items()}
        self.mask = sitk.GetArrayFromImage(mask.image)
        self.key_prefix = key_prefix
        self.coordinates = tuple(coordinates)

    def __eq__(self, other: 'Key') -> bool:
        try:
            if not (self.key_prefix == other.key_prefix and numpy.array_equal(self.mask, other.mask)):
                return False

            if not self.images.keys() == other.images.keys():
                return False

            if not self.coordinates == other.coordinates:
                return False

            for i1, i2 in zip(self.images.values(), other.images.values()):
                if not numpy.array_equal(
                    i1,
                    i2
                ):
                    return False
        except:
            return False

        return True

    def __hash__(self):
        current_hash = hash(self.key_prefix)
        current_hash ^= mmh3.hash(self.mask)
        current_hash ^= hash(self.coordinates)
        for image in self.images.values():
            current_hash ^= mmh3.hash(image)
        return current_hash

def min_max_mask_coordinates(mask: Mask) -> Tuple[Tuple[int]]:
    min_coordinates = list(mask.image.GetSize())
    max_coordinates = [0, 0, 0]

    for i in range(3):
        for index in mask.indices:
            if mask.image[index] == 1:
                if index[i] > max_coordinates[i]:
                    max_coordinates[i] = index[i]
                if index[i] < min_coordinates[i]:
                    min_coordinates[i] = index[i]
    return tuple(min_coordinates), tuple(max_coordinates)


class CoordinateScaler:
    def __init__(self, mask: Mask):
        self.min, self.max = min_max_mask_coordinates(mask)

    def __call__(self, coordinates):
        return tuple(map(lambda i: i[0] - i[1], zip(coordinates, self.min)))

    def range(self):
        return tuple(map(lambda i: i[1] - i[0] + 1, zip(self.min, self.max)))


class CachedFeatures(Features):
    """
    Dekorator dla klasy cech. Wykorzystuje dyskową pamięć podręczną w celu uniknięcia nadmiarowych obliczeń.
    Jeśli wyznaczane z wykorzystaniem tej klasy cechy znajdują się w pamięci podręcznej, to są z niej
    pobierane. W przeciwnym razie cechy są obliczane z wykorzystaniem dekorowango obiektu klasy `Features`
    i zapisywane do pamięci podręcznej. Klucze w pamięci podręcznej wyznaczane są na podstawie dekorowanego obiektu
    obrazów, maski i prefiksu klucza.
    """
    def __init__(
            self,
            features: Features,
            images: Iterable[sitk.Image],
            mask: Mask,
            cache: diskcache.Cache,
            key_prefix: str
    ):
        """
        Konstruktor inicjalizujący pola klasy.


        :param features: dekorowany obiekt cech
        :param images: obrazy
        :param mask: obiekt maski
        :param cache: obiekt dyskowej pamięci podręcznej
        :param key_prefix: prefiks klucza, powinien być unikalny dla przekazanego obiektu cech
        """
        self._features = features
        self._mask = mask
        self._cache = cache
        self._logger = logging.getLogger(__name__)
        self._coordinate_scaler = CoordinateScaler(mask)

        key = key_prefix
        for i in images:
            array = sitk.GetArrayFromImage(i)
            hash_value = mmh3.hash(array)
            key = f"{key}_{hash_value}"

        mask_hash = mmh3.hash(sitk.GetArrayFromImage(mask.image))
        key = f"{key}_{mask_hash}"
        self._key = key

    @lazy
    def _features_array(self) -> numpy.ndarray:
        self._logger.debug(f"Features key: {self._key}")
        if self._key in self._cache:
            self._logger.info(f"Found key in cache, reading cached features...")
            return self._cache[self._key]
        else:
            self._logger.info(f"Key not found in cache, calculating all features...")
            features_array = numpy.full((*self._coordinate_scaler.range(), self.dimensions), numpy.nan)
            for index in tqdm(self._mask.indices, desc="Calculating features"):
                features_array[self._coordinate_scaler(index)] = self._features[index]
            self._cache[self._key] = features_array
            return features_array

    @property
    def size(self) -> int:
        return self._features.size

    def header(self) -> List['FeatureHeader']:
        return self._features.header()

    def __getitem__(self, coordinates):
        return self._features_array[self._coordinate_scaler(coordinates)]
