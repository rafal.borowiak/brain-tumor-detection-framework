
class FeatureHeader:
    def __init__(self, image_type: 'ImageType', feature_set: str, feature_name: str):
        self.image_type = image_type
        self.feature_set = feature_set
        self.feature_name = feature_name
