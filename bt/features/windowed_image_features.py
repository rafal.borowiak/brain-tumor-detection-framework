from typing import Dict

import SimpleITK as sitk

from bt.mask import Mask
from bt.images.image_type import ImageType
from .features_calculator import FeaturesCalculator
from .windowed_features import WindowedFeatures


class WindowedImageFeatures(WindowedFeatures):

    def __init__(
            self,
            images: Dict[ImageType, sitk.Image],
            mask: Mask,
            features_calculator: FeaturesCalculator,
            name: str,
            window_size: int = 3
    ):
        super().__init__(
            images=images,
            mask=mask,
            features_calculator=features_calculator,
            name=name,
            window_size=window_size
        )

    def __getitem__(self, coordinates):
        coordinates_slices = self.coordinates_slices(coordinates)

        features = []
        mask_window = self.mask.image.__getitem__(*coordinates_slices)

        for image in self.images.values():
            image_window = image.__getitem__(*coordinates_slices)
            features += self.features_calculator(image_window, mask_window)

        return features
