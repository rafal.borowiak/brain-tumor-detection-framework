import numpy
import numpy.ma
from numba import jit
from typing import List

from bt.features.features_calculator import FeaturesCalculator
from bt.features.windowed_numpy_array_features import WindowedNumpyArrayFeatures
from bt.mask import Mask
import SimpleITK as sitk
import numpy as np


@jit(nopython=True)
def _calculate_histogram(array: numpy.ndarray, number_of_bins: int, limit: int):
    histogram, bins = numpy.histogram(array, number_of_bins, range=(0, limit))
    return list(histogram)


class Histogram(FeaturesCalculator):
    def __init__(self, number_of_bins, limit):
        self.number_of_bins = number_of_bins
        self.limit = limit

    def names(self) -> List[str]:
        return [f"bin {i}" for i in range(self.number_of_bins)]

    def __call__(self, image: numpy.ndarray, mask: numpy.ndarray):
        masked = numpy.ma.array(data=image, mask=numpy.logical_not(mask)).compressed()
        return _calculate_histogram(masked, self.number_of_bins, self.limit)


class HistogramFeatures(WindowedNumpyArrayFeatures):
    def __init__(self, images: [sitk.Image], mask: Mask, number_of_bins: int, window_size: int):
        self.max = 100
        super().__init__(
            images=images,
            mask=mask,
            features_calculator=Histogram(number_of_bins, self.max),
            name="histogram",
            window_size=window_size
        )
        self.arrays = list(map(self._scale, self.arrays))

    def _scale(self, array: np.ndarray):
        array -= array.min()
        array = array.astype(float)
        array *= float(self.max) / float(array.max())
        array = array.astype(int)
        return array