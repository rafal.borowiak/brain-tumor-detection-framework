from typing import Dict
import SimpleITK as sitk

from jsonobject import *

from bt.images.image_type import ImageType


class BaseFeaturesConfiguration(JsonObject):
    image_types = ListProperty()

    def _features_constructor(self, images: Dict[ImageType, sitk.Image], mask: 'Mask'):
        raise NotImplementedError()

    def _features_constructor_mapping(self, images: Dict[ImageType, sitk.Image], mask: 'Mask'):
        images = {t: images[t] for t in map(lambda s: ImageType[s.upper()], self.image_types)}
        return self._features_constructor(images, mask)

    def create_features_constructor(self):
        return self._features_constructor_mapping
