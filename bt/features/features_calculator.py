from abc import abstractmethod, ABC
from typing import List

import numpy


class FeaturesCalculator(ABC):
    """
    Klasa bazowa dla kalkulatora cech z okna.
    """

    @abstractmethod
    def names(self) -> List[str]:
        raise NotImplementedError()

    @abstractmethod
    def __call__(self, array: numpy.ndarray, mask: numpy.ndarray) -> List[float]:
        """
        Operator wywołania, wyznacza wektor cech dla przekazanego okna i maski.

        :param array: okno w postaci macierzy trójwymiarowej
        :param mask: maska wyznaczająca obszar obliczanych cech
        :return: Zwraca wektor cech wyznaczonych dla przekazanego okna.
        """
        raise NotImplementedError()
