from typing import Dict

import SimpleITK as sitk

from bt.features.features_calculator import FeaturesCalculator
from .windowed_features import WindowedFeatures
import numpy
from bt.mask import Mask


class WindowedNumpyArrayFeatures(WindowedFeatures):

    def __init__(
            self,
            images: Dict['ImageType', sitk.Image],
            mask: Mask,
            features_calculator: FeaturesCalculator,
            name: str,
            window_size: int = 3
    ):
        super().__init__(
            images=images,
            mask=mask,
            features_calculator=features_calculator,
            name=name,
            window_size=window_size
        )
        self.arrays = list(map(sitk.GetArrayFromImage, images.values()))
        self.mask_array = sitk.GetArrayFromImage(mask.image)

    def __getitem__(self, coordinates):
        cooridinates_slice = tuple(reversed(self.coordinates_slices(coordinates)))

        features = []
        mask_window = self.mask_array.__getitem__(cooridinates_slice)

        for array in self.arrays:
            array_window = array.__getitem__(cooridinates_slice)
            features += self.features_calculator(array_window, mask_window)

        return numpy.array(features)
