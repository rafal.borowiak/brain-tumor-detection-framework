import numpy
import numpy.ma
from numba import jit
from typing import List

from bt.features.features_calculator import FeaturesCalculator


@jit(nopython=True, nogil=True)
def _calculate_features(array: numpy.ndarray):
    array_min = numpy.min(array)
    array_max = numpy.max(array)
    mean = numpy.mean(array)
    median = numpy.median(array)
    energy = numpy.sum((array - array_min) ** 2)

    differences = array - mean
    p2 = numpy.power(differences, 2)
    m2 = numpy.mean(p2)

    if m2 == 0:  # Flat Region
        kurtosis = 0
        skewness = 0
    else:
        p3 = p2 * differences
        p4 = p3 * differences
        m3 = numpy.mean(p3)
        m4 = numpy.mean(p4)

        kurtosis = m4 / m2 ** 2
        skewness = m3 / m2 ** 1.5

    variance = numpy.var(array)

    return [mean, variance, median, array_min, array_max, kurtosis, skewness, energy]


class StatisticalFeaturesCalculator(FeaturesCalculator):
    """
    Klasa kalkulatora cech statystycznych. Służy do wyznaczania cechy statystycznych
    z przekazanego okna.
    """

    def names(self) -> List[str]:
        return ["value", "mean", "variance", "median", "min", "max", "kurtosis", "skewness", "energy"]

    def __call__(self, array: numpy.ndarray, mask: numpy.ndarray) -> List[float]:
        value = array[array.shape[0] // 2, array.shape[1] // 2, array.shape[2] // 2]
        array = numpy.ma.array(data=array, mask=numpy.logical_not(mask)).compressed()
        return [value] + _calculate_features(array)
