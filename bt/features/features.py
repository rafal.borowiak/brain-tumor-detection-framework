from typing import List, Tuple, Iterable

from tqdm import tqdm

from .features_iterator import FeaturesIterator
from fn.iters import map, flatten
import numpy as np
from abc import ABC, abstractmethod


class FeaturesComposite:
    """
    Klasa kompozytu cech.
    """
    def __init__(self, features_list: List['Features'], config):
        """
        Konstruktor inicjalizujący pola klasy.

        :param features_list: lista obiektów klasy `Features`
        :param config: obiekt wykorzystywany jedynie w metodzie `str` do konwersji na łańcuch znaków
        """
        self.size = features_list[0].size
        self.features = features_list
        self.config = config

    def __add__(self, other: 'Features') -> 'FeaturesComposite':
        """
        Operator sumy. Tworzy nowy kompozyt cech, który zawiera cechy tego kompozytu i cechy wykorzystane po
        prawej stronie operatora.

        :param other: prawa strona operatora, dodane cechy
        :return: Zwraca kompozyt cech.
        """
        return FeaturesComposite(self.features + [other])

    def __getitem__(self, coordinates: Tuple[int, int, int]) -> List[float]:
        """
        :param coordinates: współrzędne woksela
        :return: Zwraca wektor cech dla woksela o przekazanych współrzędnych.
        """
        return list(flatten(map(lambda it: it[coordinates], self.features)))

    def __iter__(self) -> FeaturesIterator:
        """
        :return: Zwraca iterator, umożliwiający przeglądanie wektorów cech.
        """
        return FeaturesIterator(self)

    @property
    def dimensions(self) -> int:
        return len(self.header())

    def header(self) -> List['FeatureHeader']:
        return list(flatten(map(lambda it: it.header(), self.features)))

    def select(self, coordinates: Iterable[Tuple[int, int, int]]) -> np.ndarray:
        """
        Oblicza wartości cech dla przekazanych współrzędnych.

        :param coordinates: lista współrzędnych wokseli
        :return: Zwraca tablicę numpy z obliczonymi wartościami cech dla współrzędnych przekazanych w `coordinates`.
        """
        features = [self[i] for i in tqdm(coordinates, desc="Calculating features")]
        return np.array(features)

    def __str__(self):
        return str(self.config)


class Features(ABC):
    """
    Klasa bazowa dla klas cech.
    """
    @abstractmethod
    def __getitem__(self, coordinates):
        """
        :param coordinates: współrzędne woksela
        :return: Zwraca wektor cech dla woksela o przekazanych współrzędnych.
        """
        raise NotImplementedError()

    @abstractmethod
    def header(self) -> List['FeatureHeader']:
        raise NotImplementedError()

    @property
    def dimensions(self) -> int:
        return len(self.header())

    @property
    @abstractmethod
    def size(self) -> int:
        raise NotImplementedError()

    def __add__(self, other) -> FeaturesComposite:
        """
        Operator sumy. Tworzy kompozyt cech, który łączy te cechy i cechy wykorzystane po
        prawej stronie operatora.

        :param other: prawa strona operatora, dodane cechy
        :return: Zwraca kompozyt cech.
        """
        return FeaturesComposite([self] + [other])

    def __iter__(self) -> FeaturesIterator:
        """
        :return: Zwraca iterator, umożliwiający przeglądanie wektorów cech.
        """
        return FeaturesIterator(self)

    def select(self, coordinates):
        """
        Oblicza wartości cech dla przekazanych współrzędnych.

        :param coordinates: lista współrzędnych wokseli
        :return: Zwraca tablicę numpy z obliczonymi wartościami cech dla współrzędnych przekazanych w `coordinates`.
        """
        return np.array([self[i] for i in coordinates])
