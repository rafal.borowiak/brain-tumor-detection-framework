from jsonobject import *

from bt.images.image_type import ImageType
from .histogram import HistogramFeatures
from .windowed_features_configuration import WindowedFeaturesConfiguration
import SimpleITK as sitk
from typing import Dict


class HistogramConfiguration(WindowedFeaturesConfiguration):
    number_of_bins = IntegerProperty()

    def _features_constructor(self, images: Dict['ImageType', sitk.Image], mask: 'Mask'):
        return HistogramFeatures(images, mask, int(self.number_of_bins), self.window_size)
