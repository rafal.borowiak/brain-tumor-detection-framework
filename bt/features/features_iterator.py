class FeaturesIterator:
    def __init__(self, features):
        self.features = features
        self.x = 0
        self.y = 0
        self.z = 0

    def __next__(self):
        item = self.features[self.x, self.y, self.z]
        self.x += 1
        if self.x == self.features.size[0]:
            self.x = 0
            self.y += 1
        if self.y == self.features.size[1]:
            self.y = 0
            self.z += 1
        if self.z >= self.features.size[2]:
            raise StopIteration()
        return item
