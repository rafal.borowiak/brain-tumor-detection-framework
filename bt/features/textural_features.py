from .features_calculator import FeaturesCalculator
from .glcm import calculate_glcm_features, glcm_features
from .windowed_numpy_array_features import WindowedNumpyArrayFeatures
import SimpleITK as sitk
from typing import List, Dict
import numpy as np
from bt.mask import Mask


class TexturalFeaturesCalculator(FeaturesCalculator):
    """
    Klasa obliczająca cechy teksturalne.
    """
    def __init__(self, max: int):
        self.max = max

    def __call__(self, window: np.ndarray, mask: np.ndarray) -> List[float]:
        return calculate_glcm_features(window, max=self.max)

    def names(self) -> List[str]:
        return ["energy", "entropy", "contrast", "variance", "correlation", "inverse difference moment"]


class TexturalFeatures(WindowedNumpyArrayFeatures):
    """
    Klasa reprezentująca cechy teksturalne.
    """
    def __init__(self, images: Dict['ImageType', sitk.Image], mask: Mask, window_size: int):
        """
        Konstruktur inicjalizujący pola klasy.

        :param images: zestaw obrazów
        :param mask: maska, obszar w którym wyznaczane są cechy
        :param window_size: rozmiar okna, wymagana wartość nieparzysta
        """
        self.max = 99
        super().__init__(
            images=images,
            mask=mask,
            features_calculator=TexturalFeaturesCalculator(self.max),
            name="second_order",
            window_size=window_size
        )
        self.arrays = list(map(self._scale, self.arrays))

    def _scale(self, array: np.ndarray):
        array -= array.min()
        array = array.astype(float)
        array *= float(self.max) / float(array.max())
        array = array.astype(int)
        return array
