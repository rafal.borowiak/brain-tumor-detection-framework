from sklearn.base import BaseEstimator
from sklearn.feature_selection.base import SelectorMixin
import numpy as np


class RemoveDuplicateColumns:
    def __init__(self):
        self._columns_to_filter = set()

    @staticmethod
    def _are_same(first_row: np.ndarray, second_row: np.ndarray) -> bool:
        ratio = first_row / second_row
        ratio = ratio[~np.isnan(ratio)]

        if len(ratio) == 0:
            return True

        to_compare = np.array([ratio[0]] * len(ratio))

        return np.allclose(ratio, to_compare)

    def fit(self, X: np.ndarray, y=None) -> 'RemoveDuplicateColumns':
        self._columns_to_filter = set()
        transposed = np.transpose(X)

        for index, row in enumerate(transposed):
            for second_index, second_row in enumerate(transposed[index + 1:]):
                if RemoveDuplicateColumns._are_same(row, second_row):
                    self._columns_to_filter |= {second_index + index + 1}

        return self

    def transform(self, X: np.ndarray) -> np.ndarray:
        selections = [index not in self._columns_to_filter for index in range(X.shape[1])]
        return X[:, selections]

    def fit_transform(self, X: np.ndarray, y=None):
        return self.fit(X).transform(X)
