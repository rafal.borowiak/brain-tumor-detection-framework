from jsonobject import *

from bt.classifier import ClassifierConfiguration, ClassifierScaler
from bt.features import FeaturesConfiguration


class StageConfiguration(JsonObject):
    """
    Konfiguracja pojedynczego etapu segmentacji.
    """
    classifier = ObjectProperty(ClassifierConfiguration)
    features = ObjectProperty(FeaturesConfiguration)
    sample_size = IntegerProperty()

    def create_classifier_scaler(self) -> ClassifierScaler:
        """
        Tworzy obiekt pojedynczego etapu segmentacji na podstawie konfiguracji.

        :return: Obiekt reprezentujący etap segmentacji.
        """
        return ClassifierScaler(
            classifier=self.classifier.create_classifier(),
            scaler=self.features.create_preprocessing_pipeline()
        )
