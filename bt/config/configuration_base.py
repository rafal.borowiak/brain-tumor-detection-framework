from jsonobject import *

from bt.cache.cache_configuration import CacheConfiguration
from bt.mask import MaskConfiguration
from bt.images import ImagesSetConfiguration


class BaseConfiguration(JsonObject):
    """
    Klasa bazowa dla konfiguracji programu.
    """
    images = ObjectProperty(ImagesSetConfiguration)

    mask = ObjectProperty(MaskConfiguration)

    cache = ObjectProperty(CacheConfiguration)

    def __init__(self, json):
        if "sequences" in json:
            json["images"] = json["sequences"]
        super().__init__(json)
