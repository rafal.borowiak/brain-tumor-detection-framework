from typing import Tuple, Dict, Callable

from jsonobject import *
from lazy import lazy

from bt.config.configuration_base import BaseConfiguration
from bt.config.stage_configuration import StageConfiguration
from bt.features.tissue import StageClasses
from bt.images import LazyClassifiedImages, ClassifiedPatientsData, TruthImagesConfiguration
from bt.images.caching_classified_images import CachingClassifiedImages
from bt.images.sized_images_generator import SizedImagesGenerator


class TrainingConfiguration(BaseConfiguration):
    """
    Klasa reprezentująca konfigurację procesu nauczania algorytmu segmentacji wieloetapowej.
    """
    truth_images = ObjectProperty(TruthImagesConfiguration)

    first_stage = ObjectProperty(StageConfiguration)

    edema = ObjectProperty(StageConfiguration)

    core = ObjectProperty(StageConfiguration)

    model_output = StringProperty()

    def __init__(self, json):
        super().__init__(json)

    @lazy
    def reference_images(self):
        """
        Metoda zwracająca na podstawie konfiguracji zestaw obrazów odniesienia dla algorytmu dopasowania histogramu.
        Wybierane są pierwsze obrazy ze zbioru treningowego.

        :return: Obrazy odniesienia.
        """
        return self.images.first_images()

    def load_classified_patient_sequences(self) -> ClassifiedPatientsData:
        """
        Na podstawie konfiguracji ładuje obrazy pacjentów.

        :return: Obrazy pacjentów razem z obrazami posegmentowanymi.
        """
        cache = self.cache.create_cache()
        patient_images = self.images.load_patients_images(
            mask_config=self.mask,
            reference_images=self.reference_images,
            cache=cache
        )
        truth_paths = self.truth_images.resolved_paths
        truth_loader = self.truth_images.image_loader

        return ClassifiedPatientsData(
            SizedImagesGenerator(
                (CachingClassifiedImages(
                    images=LazyClassifiedImages.create(sequence, lambda path=path: truth_loader(path)),
                    cache=cache
                ) for sequence, path in zip(patient_images, truth_paths)),
                len(truth_paths)
            )
        )

    def create_features_constructors(self) -> Dict[StageClasses, Tuple[Callable, int]]:
        """
        Tworzy słownik, którego kluczami są etapy segmentacji, a wartościami pary zawierające konstruktor cech dla
        danego etapu i rozmiar zbioru treningowego dla tego etapu, na podstawie konfiguracji.

        :return: Słownik etapów segmentacji.
        """
        return {
            StageClasses.FirstStage: (self.first_stage.features.create_features_constructor(), self.first_stage.sample_size),
            StageClasses.Edema: (self.edema.features.create_features_constructor(), self.edema.sample_size),
            StageClasses.Core: (self.core.features.create_features_constructor(), self.core.sample_size)
        }
