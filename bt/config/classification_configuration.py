from typing import List

from jsonobject import *

from bt.config.configuration_base import BaseConfiguration
from bt.postprocessing import PostprocessingConfiguration
from bt.images import TruthImagesConfiguration, LazyImages
import SimpleITK as sitk

class ClassificationConfiguration(BaseConfiguration):
    """
    Klasa reprezentująca konfigurację procesu segmentacji wieloetapowej.
    """
    truth_images = ObjectProperty(TruthImagesConfiguration)
    model = StringProperty()
    result_path = StringProperty()
    report_file_path = StringProperty()
    stats_file_path = StringProperty()
    core_postprocessing = ObjectProperty(PostprocessingConfiguration)
    final_postprocessing = ObjectProperty(PostprocessingConfiguration)

    def __init__(self, json):
        super().__init__(json)

    def load_images(self, reference_images: List[sitk.Image]) -> LazyImages:
        """
        Ładuje zestaw obrazów testowych z wykorzystaniem obrazów odniesienia w celu dopasowania histogramu.
        
        :param reference_images: obrazy odniesienia do dopasowania obrazu
        :return: Zwraca zestaw leniwie ładowanych obrazów testowych.
        """
        cache = self.cache.create_cache()
        return self.images.load_patients_images(self.mask, reference_images, cache)
