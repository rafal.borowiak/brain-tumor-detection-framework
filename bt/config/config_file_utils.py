import glob
import json
import logging
import os
import sys
from logging import handlers
from typing import List, Dict

import yaml
from functional import seq


def load_dict_from_file(path: str) -> Dict:
    """
    Ładuje słownik z pliku JSON lub YAML.

    :param path: ścieżka do pliku o rozszerzeniu `.json`, `.js`, `.yaml` lub `.yml`.
    :return: Zwraca słownik wartości załadowanych z pliku.
    """
    with open(path) as file:
        content = file.read()
        if path.lower().endswith(('.json', '.js')):
            return json.loads(content)

        if path.lower().endswith((".yaml", ".yml")):
            return yaml.load(content)


def expand_jsons(root: Dict) -> Dict:
    """
    Jeśli przekazany słowik zawiera klucz będący ścieżką do piku JSON lub YAML, to w zwróconym słowniku
    pod klucz ten wpisywana jest zawartości pliku załadowana z wykorzystaniem funkcji `load_dict_from_file`.
    Funkcja działa rekurencyjnie, dla wszystkich zagnieżdżonych kluczy.

    :param root: słownik wejściowy
    :return: Zwraca słownik z podmienionymi wartościami.
    """
    for key, value in root.items():
        if isinstance(value, str) and value.lower().endswith(('.json', '.js', '.yaml', '.yml')):
            root[key] = load_dict_from_file(value)
        if isinstance(value, dict):
            root[key] = expand_jsons(value)
    return root


def config_file(path: str) -> Dict:
    """
    Funkcja ładuje plik konfiguracyjny w postaci słownika.

    :param path: ścieżka do pliku konfiguracyjnego.
    :return: Zwraca plik konfiguracyjy w postaci słownika.
    """
    config = load_dict_from_file(path)
    return expand_jsons(config)


def configs_paths() -> List[str]:
    """
    Zwraca ścieżki do plików konfiguracyjnych na podstawie przekazanych argumentów aplikacji.
    Jeśli jednym z argumentów wywołania programu był folder to także wszystkie pliki z
    folderu traktowane są jako pliki konfiguracyjne

    :return: Zwraca listę ścieżek do plików konfiguracyjnych.
    """
    return seq(sys.argv[1:]) \
        .map(lambda path: glob.glob(path + "/*") if os.path.isdir(path) else [path]) \
        .flatten() \
        .sorted() \
        .to_list()


def config_files() -> List[Dict]:
    """
    :return: Zwraca listę słowników z wartościami konfiguracyjnymi na podstawie ścieżek uzyskanych wywołaniem
    funkcji `configs_paths`.
    """
    return seq(configs_paths()) \
        .map(config_file) \
        .to_list()


def configure_loggers():
    """
    Ustawia konfigurację klas odpowiadających za logi aplikacji.
    """
    log = logging.getLogger('')
    log.setLevel(logging.DEBUG)
    format = logging.Formatter(
        fmt='[%(levelname)s] [%(asctime)s] %(message)s',
        datefmt='%H:%M:%S'
    )

    stdoutHandler = logging.StreamHandler(sys.stdout)
    stdoutHandler.setFormatter(format)

    log.addHandler(stdoutHandler)

    fileHandler = handlers.RotatingFileHandler('log.log', maxBytes=(1048576 * 5), backupCount=7)
    fileHandler.setFormatter(format)
    log.addHandler(fileHandler)