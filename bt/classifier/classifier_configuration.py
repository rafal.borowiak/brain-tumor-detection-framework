from functional import seq
from jsonobject import *

from sklearn.svm import SVC
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, GradientBoostingClassifier, \
    AdaBoostClassifier, BaggingClassifier, VotingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.tree import DecisionTreeClassifier


def _create_adaboost_classifier(**kwargs):
    if kwargs is not None and "base_estimator" in kwargs.keys():
        kwargs["base_estimator"] = ClassifierConfiguration(kwargs["base_estimator"]).create_classifier()
    return AdaBoostClassifier(**kwargs)


def _create_bagging_classfier(**kwargs):
    if kwargs is not None and "base_estimator" in kwargs.keys():
        kwargs["base_estimator"] = ClassifierConfiguration(kwargs["base_estimator"]).create_classifier()
    return BaggingClassifier(**kwargs)


def _create_voting_classifier(**kwargs):
    if kwargs is not None and "estimators" in kwargs.keys():
        kwargs["estimators"] = seq(kwargs["estimators"]) \
            .map(lambda c: ClassifierConfiguration(c).create_classifier()) \
            .map(lambda c: (str(c), c)) \
            .to_list()
    return VotingClassifier(**kwargs)


class ClassifierConfiguration(JsonObject):
    """
    Obiekt określający typ i parametry klasyfikatora w konfiguracji.
    """
    type = StringProperty()
    parameters = DictProperty(default={})

    def create_classifier(self):
        """
        Tworzy klasyfikator na podstawie ustawionego typu i parametrów.
        :return: Utworzony obiekt klasyfikatora.
        """
        factories = {
            "svm": SVC,
            "extra_trees": ExtraTreesClassifier,
            "neural_network": MLPClassifier,
            "knn": KNeighborsClassifier,
            "bayes": GaussianNB,
            "random_forest": RandomForestClassifier,
            "decision_tree": DecisionTreeClassifier,
            "gradient_boosting": GradientBoostingClassifier,
            "ada_boost": _create_adaboost_classifier,
            "voting_classifier": _create_voting_classifier,
            "bagging_classifier": _create_bagging_classfier,
            "gaussian_process": GaussianProcessClassifier
        }

        return factories[self.type.lower()](**self.parameters)
