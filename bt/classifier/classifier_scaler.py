import logging

import numpy


class ClassifierScaler:
    """
    Fasada dla obiektu przekształcającego wektor cech i obiektu klasyfikatora.
    Reprezentuje pojedynczy etap segmentacji wieloetapowej.
    """
    def __init__(self, classifier, scaler):
        self.classifier = classifier
        self.scaler = scaler

    def fit(self, X: numpy.ndarray, y: numpy.ndarray) -> 'ClassifierScaler':
        """
        Przeprowadza proces uczenia obiektu przekształcającego wektor cech i klasyfikatora.

        :param X: tablica wektorów cech
        :param y: tablica klas wektorów cech
        :return: Zwraca referencje do samego siebie.
        """
        logger = logging.getLogger(__name__)

        logger.info("Scaling features vector...")
        scaled_features = self.scaler.fit_transform(X, y)

        logger.info(f"Features vector shape after scaling: {scaled_features.shape}")
        logger.info(f"Training classifier...")
        self.classifier.fit(scaled_features, y)

        return self

    def predict(self, X: numpy.ndarray) -> numpy.ndarray:
        """
        Skaluje i klasyfikuje przekazaną tablicę wektorów cech.

        :param X: zbiór wektorów cech.
        :return: Zwraca wektor klas przypisanych kolejnym wektorom cech.
        """
        logger = logging.getLogger(__name__)

        logger.info("Scaling features vector...")
        scaled_features = self.scaler.transform(X)
        logger.info(f"Features vector shape after scaling: {scaled_features.shape}")

        logger.info("Classifing...")
        return self.classifier.predict(scaled_features)
