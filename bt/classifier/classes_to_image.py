import SimpleITK as sitk
import numpy
from tqdm import tqdm

from bt.mask import Mask


def create_image_from_classes(mask: Mask, classes: numpy.ndarray) -> sitk.Image:
    """
    Funkcja tworząca obrazy na podstawie przekazanej maski i macierzy z wartościami klas.

    :param mask: obraz maski
    :param classes: trójwymiarowa macierz z wartościami klas
    :return: Obraz stworzony na podstawie przekazanej maski i macierzy.
    """
    result = sitk.Image(
        *mask.image.GetSize(),
        sitk.sitkUInt16
    )

    result *= 0

    for index, clas in tqdm(list(zip(mask.indices, classes)), desc="Creating image from classes"):
        result[index] = int(clas)

    return result
