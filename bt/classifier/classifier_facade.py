import logging
from typing import List, Tuple

import SimpleITK as sitk

from bt.classifier.classes_to_image import create_image_from_classes
from bt.config.classification_configuration import ClassificationConfiguration
from bt.features import Classes
from bt.classifier.multistage_classifier import MultistageClassifier
from bt.images.images import Images
from bt.images.lazy_images import LazyImages
from bt.mask import Mask


class ClassifierFacade:
    """
    Klasa umożliwiająca przeprowadzenie pełnego procesu segmentacji wieloetapowej na podstawie konfiguracji.
    Obiekt klasy ClassifierFacade jest iterowalny.
    """
    def __init__(self, config: ClassificationConfiguration):
        self.config = config

        self.model = MultistageClassifier.load(config.model)
        self.model_config = self.model.config
        self.logger = logging.getLogger(__name__)
        self.images = config.load_images(self.model.references)
        self.core_postprocessing = config.core_postprocessing.create_postprocessing_pipeline()
        self.final_postprocessing = config.final_postprocessing.create_postprocessing_pipeline()

    def classify_image(self, images: LazyImages) -> Tuple[LazyImages, Classes]:
        """
        Metoda przeprowadzająca proces segmentacji wieloetapowej na podstawie przekazanych obrazów.
        :param images: obrazy jednego pacjenta
        :return: Zwraca parę zawierająca przekazane obrazy i obiekt wyznaczonych klas.
        """
        self.logger.info(f"Calculating features {images.paths}...")
        classification = \
            self.model.first_stage.predict(
                images.masked_features(
                    self.model.config.first_stage.features.create_features_constructor()
                )
            )

        self.logger.info(f"Creating result image...")
        first_stage_result = create_image_from_classes(mask=images.mask, classes=classification)

        self.logger.info(f"Performing postprocessing...")
        first_stage_result = self.core_postprocessing(first_stage_result)

        core_mask = Mask(first_stage_result)
        core_images = Images(images=images.images, paths=images.paths, mask=core_mask)

        core_classification = \
            self.model.core.predict(
                core_images.masked_features(
                    self.model.config.core.features.create_features_constructor()
                )
            )

        edema_mask = images.mask - core_mask
        edema_images = Images(images=images.images, paths=images.paths, mask=edema_mask)

        edema_classification = \
            self.model.edema.predict(
                edema_images.masked_features(
                    self.model.config.edema.features.create_features_constructor()
                )
            )

        result = sitk.Image(*first_stage_result.GetSize(), sitk.sitkInt16)
        for clas, index in zip(edema_classification, edema_mask.indices):
            result[index] = int(clas)

        for clas, index in zip(core_classification, core_mask.indices):
            result[index] = int(clas)

        result = self.final_postprocessing(result)

        return images, Classes(result, images.mask, number_of_classes=5)

    def __iter__(self):
        """
        Metoda dostępu do iteratora, który leniwie wykonuje proces segmentacji na obrazach przekazanych w~konfiguracji.
        :return: Zwraca iterator, za pomocą którego można przeglądać obrazy i~klasy do nich przypisane.
        """
        return map(self.classify_image, self.images)
