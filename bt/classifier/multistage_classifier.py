import gzip
import logging
import pickle
from typing import List

import SimpleITK as sitk

from bt.classifier import ClassifierScaler
from bt.config import TrainingConfiguration


class MultistageClassifier:
    """
    Klasa, której celem jest przechowywanie obiektów reprezentujących każdy z etapów segmentacji,
    obrazów odniesienia oraz konfiguracji treningowej.
    """
    def __init__(
            self,
            first_stage: ClassifierScaler,
            core: ClassifierScaler,
            edema: ClassifierScaler,
            references: List[sitk.Image],
            config: TrainingConfiguration
    ):
        self.logger = logging.getLogger(__name__)
        self.first_stage = first_stage
        self.core = core
        self.edema = edema
        self.references = references
        self.config = config

    def save(self):
        """
        Zapisuje etapy segmentacji do skompresowanego pliku. Ścieżka do pliku wynika z konfiguracji.
        """
        self.logger.info(f"Saving model to file {self.config.model_output}...")
        with gzip.open(self.config.model_output, "wb") as file:
            reference_arrays = list(map(sitk.GetArrayFromImage, self.references))
            data = {
                "first_stage": self.first_stage,
                "core": self.core,
                "edema": self.edema,
                "references": reference_arrays,
                "config": self.config.to_json()
            }
            raw_data = pickle.dumps(data)
            file.write(raw_data)

    @staticmethod
    def load(path: str):
        """
        Metoda statyczna odczytująca obiekt `MultistageClassifier` z pliku.
        :param path: ścieżka do pliku
        :return: Zdekodowany obiekt `MultistageClassifier`.
        """
        with gzip.open(path, "rb") as file:
            raw_data = file.read()
            data = pickle.loads(raw_data)
            data["config"] = TrainingConfiguration(data["config"])
            data["references"] = list(map(sitk.GetImageFromArray, data["references"]))
            return MultistageClassifier(**data)
