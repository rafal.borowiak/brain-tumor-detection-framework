from typing import Callable

import SimpleITK as sitk
from fn import F
from fn.op import flip
from jsonobject import *

from bt.filters import clear_small_blobs


class PostprocessingConfiguration(JsonObject):
    median_filter_window = IntegerProperty(default=0)
    clear_small_blobs = BooleanProperty(default=False)

    def create_postprocessing_pipeline(self) -> Callable[[sitk.Image], sitk.Image]:
        predicate_functions = [
            (self.median_filter_window > 0, F(flip(sitk.Median), (self.median_filter_window,) * 3)),
            (self.clear_small_blobs, clear_small_blobs)
        ]

        pipeline = F()

        for predicate, callback in predicate_functions:
            if predicate:
                pipeline >>= callback

        return pipeline