from typing import List, Callable

import SimpleITK as sitk
from jsonobject import *
from lazy import lazy

from .images_configuration_base import ImagesConfigurationBase
from .paths_utils import resolve_globs


class TruthImagesConfiguration(ImagesConfigurationBase):
    """
    Konfiguracja obrazów z segmentacją.
    """
    paths = ListProperty()

    @lazy
    def resolved_paths(self) -> List[str]:
        return resolve_globs(self.paths, self.base_directory)

    def load_images(self) -> List[sitk.Image]:
        """
        Metoda ładująca obrazy.

        :return: Zwraca listę załadowanych obrazów z segmentacją na podstawie konfiguracji.
        """
        return [
            self.image_loader(path) for path in self.resolved_paths
        ]
