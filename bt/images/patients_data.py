import logging
from collections import Counter
from typing import Dict, Callable, Set, Union, Tuple

import SimpleITK as sitk
import numpy

from bt.features import Features
from bt.features.features_classes import FeaturesClasses
from bt.mask import Mask
from bt.images.sized_images_generator import SizedImagesGenerator
from bt.images.image_type import ImageType


class ClassifiedPatientsData:
    """
    Klasa reprezentująca zestaw obrazów treningowych.
    """
    def __init__(self, patients_data: SizedImagesGenerator):
        """
        Konstruktor inicjalizujący pola klasy.

        :param patients_data: generator obektów klasy `ClassifiedImages`
        """
        self._patients_data = patients_data
        self.logger = logging.getLogger(__name__)

    def __len__(self) -> int:
        return len(self._patients_data)

    def random_features_and_classes(
            self,
            features_constructors: Dict[
                Set[Union[Set[int], int]],
                Tuple[
                    Callable[[Dict[ImageType, sitk.Image], Mask], Features],
                    int
                ]
            ]
    ) -> Dict[Set[Union[Set[int], int]], FeaturesClasses]:
        result = {}

        for images in self._patients_data:
            for allowed_classes, (features_constructor, count) in features_constructors.items():
                features, classes = images.random_class_fair_features_and_classes(
                    features_constructor=features_constructor,
                    count=count // len(self._patients_data),
                    classes=allowed_classes
                )
                self.logger.info(f"Features shape: {features.shape}")
                self.logger.info(f"Classes distribution: {Counter(classes)}")
                if len(classes) > 0:
                    if allowed_classes in result:
                        result[allowed_classes].features = numpy.append(
                            result[allowed_classes].features,
                            features,
                            axis=0
                        )
                        result[allowed_classes].classes = numpy.append(
                            result[allowed_classes].classes,
                            classes,
                            axis=0
                        )
                    else:
                        result[allowed_classes] = FeaturesClasses(features=features, classes=classes)

        return result
