from abc import ABC, abstractmethod
from typing import Dict, Callable
import SimpleITK as sitk

from bt.features import Features
from bt.mask import Mask
from bt.images.image_type import ImageType
import numpy


class ImagesBase(ABC):
    """
    Klasa bazowa reprezentująca zestaw wyrównanych obrazów jednego pacjenta.
    """
    @property
    @abstractmethod
    def mask(self) -> Mask:
        pass

    @property
    @abstractmethod
    def images(self) -> Dict[ImageType, sitk.Image]:
        pass

    @property
    @abstractmethod
    def paths(self) -> Dict[ImageType, str]:
        pass

    def masked_features(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features]
    ) -> numpy.ndarray:
        """
        Zwraca zbiór wektorów cech wewnątrz maski wyznaczonych z wykorzystaniem przekazanej funkcji.

        :param features_constructor: funkcja wyznaczająca cechy na podstawie obrazów i maski
        :return: Zwraca zbiór wektorów cech w postaci macierzy `numpy`.
        """
        features = features_constructor(self.images, self.mask)
        return features.select(self.mask.indices)
