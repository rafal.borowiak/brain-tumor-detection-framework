import random
from typing import Callable, List, Iterable, Union, Dict

import SimpleITK as sitk
from functional import seq
from lazy import lazy

from bt.features import Classes
from bt.features import Features
from bt.mask import Mask
from bt.images.classified_images import ClassifiedImages
from bt.images.images_base import ImagesBase
from bt.images.image_type import ImageType
from .lazy_images import LazyImages


class LazyClassifiedImages(ClassifiedImages):
    """
    Klasa reprezentująca zestaw obrazów wraz z obrazem segmentowanym leniwie ładowanym z dysku.
    """
    def __init__(
            self,
            images: ImagesBase,
            classification_loader: Callable[[], sitk.Image]
    ):
        """
        Konstruktor inicjalizujący pola klasy.

        :param images: dekorowana klasa obrazów, zwykle `LazyImages`.
        :param classification_loader: funkcja ładująca obraz z segmentacją
        """
        self._images = images
        self.classification_loader = classification_loader

    @staticmethod
    def create(
            images: LazyImages,
            classification_loader: Callable[[], sitk.Image]
    ):
        return LazyClassifiedImages(
            images=images,
            classification_loader=classification_loader
        )

    @property
    def paths(self) -> Dict[ImageType, str]:
        return self._images.paths

    @property
    def mask(self) -> Mask:
        return self._images.mask

    @property
    def images(self) -> Dict[ImageType, sitk.Image]:
        return self._images.images

    @lazy
    def classes(self):
        return Classes(self.classification_loader(), self.mask, number_of_classes=5)

    def random_class_fair_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features],
            count: int,
            classes: List[Union[int, Iterable[int]]]
    ):
        number_of_classes = len(classes)
        class_coordinates = seq(classes) \
            .map(lambda clas: self.classes.random_coordinates_of_class(count//number_of_classes, clas)) \
            .to_list()

        new_count = seq(class_coordinates) \
            .map(lambda i: len(i)) \
            .min()

        for i in range(number_of_classes):
            class_coordinates[i] = random.sample(class_coordinates[i], k=new_count)

        coordinates = seq(class_coordinates).flatten().to_list()

        features = features_constructor(self.images, self.mask)

        return features.select(coordinates), self.classes.select(coordinates)

    def masked_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features]
    ):
        features = features_constructor(self.images, self.mask)
        return features.select(self.mask.indices), self.classes.select(self.mask.indices)
