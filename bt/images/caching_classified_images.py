from typing import List, Dict, Callable, Union, Iterable

import SimpleITK as sitk

from bt.features import Features, Classes
from bt.images.caching_images import CachingImages
from bt.images.classified_images import ClassifiedImages
from bt.mask import Mask
from bt.images.image_type import ImageType


class CachingClassifiedImages(CachingImages, ClassifiedImages):
    """

    """
    def random_class_fair_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features],
            count: int,
            classes: List[Union[int, Iterable[int]]]
    ):
        features = features_constructor(self.images, self.mask)
        key = f"random_class_fair_features_and_classes_{self._key}_{count}_{classes}_{str(features)}"
        self._logger.debug(f"Features key: {key}")
        if key in self.cache:
            self._logger.info("Reading cached features!")
            return self.cache[key]
        else:
            self._logger.info("Features not found in cache!")
            features_classes = self._images.random_class_fair_features_and_classes(features_constructor, count, classes)
            self.cache[key] = features_classes
            return features_classes

    @property
    def classes(self) -> Classes:
        return self._images.classes

    def masked_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features]
    ):
        features = features_constructor(self.images, self.mask)
        key = f"masked_features_and_classes_{self._key}_{str(features)}"
        self._logger.debug(f"Features key: {key}")
        if key in self.cache:
            self._logger.info("Reading cached features!")
            return self.cache[key]
        else:
            self._logger.info("Features not found in cache!")
            features_classes = self._images.masked_features_and_classes(features_constructor)
            self.cache[key] = features_classes
            return features_classes
