import logging

import diskcache

from bt.images.images_base import ImagesBase
from typing import Dict, Callable
import SimpleITK as sitk

from bt.features import Features
from bt.mask import Mask
from bt.images.image_type import ImageType
import numpy
import mmh3


class CachingImages(ImagesBase):
    def __init__(self, images: ImagesBase, cache: diskcache.Cache):
        self._images = images
        self.cache = cache
        self._logger = logging.getLogger(__name__)

        mask_hash = mmh3.hash(sitk.GetArrayFromImage(self.mask.image))
        key = f"{mask_hash}"

        for i in self.images.values():
            array = sitk.GetArrayFromImage(i)
            hash_value = mmh3.hash(array)
            key = f"{key}_{hash_value}"

        self._key = key

    @property
    def mask(self) -> Mask:
        return self._images.mask

    @property
    def paths(self) -> Dict[ImageType, str]:
        return self._images.paths

    @property
    def images(self) -> Dict[ImageType, sitk.Image]:
        return self._images.images

    def masked_features(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features]
    ) -> numpy.ndarray:
        features = features_constructor(self.images, self.mask)
        key = f"masked_features_{self._key}_{str(features)}"
        self._logger.debug(f"Features key: {key}")
        if key in self.cache:
            self._logger.info("Reading cached features!")
            return self.cache[key]
        else:
            self._logger.info("Features not found in cache!")
            masked_features = self._images.masked_features(features_constructor)
            self.cache[key] = masked_features
            return masked_features


