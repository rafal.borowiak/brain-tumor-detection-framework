from typing import Dict

from bt.images.images_base import ImagesBase
from bt.images.image_type import ImageType
import SimpleITK as sitk
from bt.mask import Mask


class Images(ImagesBase):
    """
    Klasa przechowująca pojedynczy zestaw obrazów. Przechowuje obrazy, ich ścieżki plikowe i maskę.
    """
    def __init__(
            self,
            images: Dict[ImageType, sitk.Image],
            paths: Dict[ImageType, str],
            mask: Mask
    ):
        """
        Konstruktor inicjalizujący pola klasy.

        :param images: zestaw obrazów
        :param paths: zestaw ścieżek obrazów
        :param mask: obraz maski
        """
        self._images = images
        self._paths = paths
        self._mask = mask

    @property
    def images(self) -> Dict[ImageType, sitk.Image]:
        return self._images

    @property
    def paths(self) -> Dict[ImageType, str]:
        return self._paths

    @property
    def mask(self) -> Mask:
        return self._mask
