from aenum import Enum, auto


class ImageType(Enum):
    """
    Typ wyliczeniowy określający typ obrazu.

    Przyjmowane wartości:

    \\begin{itemize}
        \item `T1C`
        \item `T1`
        \item `T2`
        \item `FLAIR`
    \end{itemize}
    """
    T1C = auto()
    T1 = auto()
    T2 = auto()
    FLAIR = auto()
