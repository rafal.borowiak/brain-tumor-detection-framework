from typing import List, Callable, Union

from jsonobject import *
from lazy import lazy

from bt.preprocessing.preprocessing_configuration import PreprocessingConfiguration
import SimpleITK as sitk


class ImagesConfigurationBase(JsonObject):
    base_directory = StringProperty(default="")
    preprocessing = ObjectProperty(PreprocessingConfiguration)

    @lazy
    def image_loader(self) -> Callable[[str], sitk.Image]:
        preprocessing = self.preprocessing.create_preprocessing_chain()
        return lambda path: preprocessing(sitk.ReadImage(path))
