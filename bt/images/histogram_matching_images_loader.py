import logging
from typing import List, Callable
import SimpleITK as sitk

from bt.preprocessing.histogram_matching import match_histograms


class HistogramMatchingImagesLoader:
    def __init__(self, image_loader: Callable[[str], sitk.Image], reference_images: List[sitk.Image]):
        self.image_loader = image_loader
        self.reference_images = reference_images
        self.logger = logging.getLogger(__name__)

    def __call__(self, paths: List[str]) -> List[sitk.Image]:
        images = list(map(self.image_loader, paths))
        self.logger.info(f"Matching histograms of {paths}...")
        return match_histograms(images, references=self.reference_images)