from abc import abstractmethod
from typing import Callable, Dict, List, Union, Iterable, Tuple

from bt.features import Classes, Features
from bt.mask import Mask
from bt.images.images_base import ImagesBase
from bt.images.image_type import ImageType
import SimpleITK as sitk
import numpy


class ClassifiedImages(ImagesBase):
    """
    Abstrakcyjna klasa bazowa reprezentująca obrazy, dla których znany jest wynik segmentacji.
    """
    @property
    @abstractmethod
    def classes(self) -> Classes:
        pass

    def class_indicies(self, clas: int):
        return self.classes.class_indices[clas]

    @abstractmethod
    def random_class_fair_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features],
            count: int,
            classes: List[Union[int, Iterable[int]]]
    ) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """
        Metoda generująca losową próbkę cech i klas, gdzie próbka jest wybierana w taki sposób, aby
        do każdego z przekazanych zestawów klas należała taka sama liczba wektorów cech.
        Przykładowo jeśli `classes = [0, [1, 2]]` i `count = 100` to 50 wygenerowanych wektorów cech będzie
        należeć do klasy 0 i 50 do klasy 1 lub 2.

        :param features_constructor: funkcja tworząca wektory cech
        :param count: liczba próbki
        :param classes: zestawy klas, dla których próbka ma być wygenerowana
        :return: Zwraca parę, której pierwszy element to tablica wygenerowanych wektorów cech, a
        drugi to tablica przypisanych wektorom cech klas.
        """
        pass

    @abstractmethod
    def masked_features_and_classes(
            self,
            features_constructor: Callable[[Dict[ImageType, sitk.Image], Mask], Features]
    ) -> Tuple[numpy.ndarray, numpy.ndarray]:
        """
        Metoda generująca wszystkie wektory cech i klasy w obszarze maski.

        :param features_constructor: funkcja tworząca wektory cech
        :return: Zwraca parę, której pierwszy element to tablica wygenerowanych wektorów cech, a
        drugi to tablica przypisanych wektorom cech klas.
        """
        pass
