import logging
from typing import Callable, List, Dict

import SimpleITK as sitk
from lazy import lazy

from bt.mask import Mask
from bt.images.images_base import ImagesBase
from bt.images.image_type import ImageType


class LazyImages(ImagesBase):
    """
    Klasa reprezentująca zestaw obrazów leniwie ładowanych z dysku.
    """
    def __init__(
            self,
            paths: Dict[ImageType, str],
            images_loader: Callable[[List[str]], List[sitk.Image]],
            mask_constructor: Callable[[List[str], List[sitk.Image]], Mask]
    ):
        """
        Konstruktor inicjalizujący pola klasy.

        :param paths: ścieżki do obrazów
        :param images_loader: funkcja ładująca obrazy
        :param mask_constructor: funkcja tworząca maskę
        """
        self._paths = paths
        self.mask_constructor = mask_constructor
        self.images_loader = images_loader
        self.logger = logging.getLogger(__name__)

    @property
    def paths(self) -> Dict[ImageType, str]:
        return self._paths

    @lazy
    def mask(self) -> Mask:
        return self.mask_constructor(list(self.paths.values()), list(self.images.values()))

    @lazy
    def images(self) -> Dict[ImageType, sitk.Image]:
        images = self.images_loader(list(self.paths.values()))
        return {image_type: image for image_type, image in zip(self.paths.keys(), images)}
