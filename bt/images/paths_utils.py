from glob import glob
from fn import iters


def _expand_and_sort(path: str, base_path: str) -> [str]:
    glob_result = glob(path)

    if len(glob_result) == 0:
        return [base_path + path]

    return [
        base_path + p for p in sorted(glob(path))
            ]


def resolve_globs(sequence: [str], base_path: str = "") -> [str]:
    return list(
        iters.flatten(
            [_expand_and_sort(path, base_path) for path in sequence]
        )
    )


def assert_equal_lengths(sequences: [[str]], message: str):
    if len(sequences) == 0:
        raise ValueError(f"Paths cannot be empty! {message}")

    expected_length = len(sequences[0])

    if not all(map(lambda x: len(x) == expected_length, sequences)):
        raise ValueError(f"Path vectors must have the same length! {message}")
