from typing import Generator, Iterator

from .classified_images import ClassifiedImages


class SizedImagesGenerator(object):
    """
    Generator obiektów klasy `ClassifiedImages`.
    """
    def __init__(self, gen: Generator[ClassifiedImages, None, None], length: int):
        """
        Konstruktor inicjalizujący pola klasy.

        :param gen: funkcja generatora
        :param length: liczba obiektów generowanych przez `gen`
        """
        self._gen = gen
        self._length = length

    def __len__(self) -> int:
        """
        :return: Zwraca liczbę obiektów, które zostaną wygenerowane przez ten generator.
        """
        return self._length

    def __iter__(self) -> Iterator[ClassifiedImages]:
        """
        Metoda umożliwiająca iterowanie po wygenerowanych obrazach.

        :return: Zwraca iterowalny obiekt generatora.
        """
        return self._gen
