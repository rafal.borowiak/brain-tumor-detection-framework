from typing import List

import SimpleITK as sitk
import diskcache
from jsonobject import *
from lazy import lazy

from bt.mask.mask_configuration import MaskConfiguration
from bt.images.caching_images import CachingImages
from bt.images.image_type import ImageType
from .histogram_matching_images_loader import HistogramMatchingImagesLoader
from .images_configuration_base import ImagesConfigurationBase
from .lazy_images import LazyImages
from .paths_utils import resolve_globs


class ImagesSetConfiguration(ImagesConfigurationBase):
    """
    Konfiguracja ścieżek do obrazów.
    """
    paths = DictProperty()

    def __init__(self, json):
        super().__init__(json)

    @lazy
    def resolved_paths(self) -> List[List[str]]:
        return [resolve_globs(paths, self.base_directory) for paths in self.paths.values()]

    @lazy
    def image_types(self) -> List[ImageType]:
        return [ImageType[type.upper()] for type in self.paths.keys()]

    def first_images(self) -> List[sitk.Image]:
        paths = list(zip(*self.resolved_paths))[0]
        return list(map(self.image_loader, paths))

    def _create_patient_sequence(
            self,
            paths: List[str],
            mask_config: MaskConfiguration,
            reference_images: List[sitk.Image],
            cache: diskcache.Cache
    ) -> CachingImages:
        return CachingImages(
            images=LazyImages(
                    paths=dict(zip(self.image_types, paths)),
                    images_loader=HistogramMatchingImagesLoader(self.image_loader, reference_images),
                    mask_constructor=mask_config.read_or_create
                ),
            cache=cache
        )

    def load_patients_images(
            self,
            mask_config: MaskConfiguration,
            reference_images: List[sitk.Image],
            cache: diskcache.Cache
    ) -> LazyImages:
        zipped_paths = zip(*self.resolved_paths)

        for paths in zipped_paths:
            yield self._create_patient_sequence(
                    list(paths),
                    mask_config,
                    reference_images,
                    cache
                )
