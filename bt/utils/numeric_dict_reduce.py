import functools
from typing import Dict, List, Set

import numpy as np


def common_keys(dicts: List[Dict]) -> Set:
    keys = list(map(lambda it: set(it.keys()), dicts))
    return functools.reduce(lambda x, y: x.intersection(y), keys)


def dict_concat(dicts: List[Dict]) -> Dict:
    result = {}
    for key in common_keys(dicts):
        values = list(map(lambda d: d[key], dicts))
        if isinstance(values[0], dict):
            result[key] = dict_concat(values)
        else:
            result[key] = values
    return result


def dict_reduce(dicts: Dict, callback) -> Dict:
    result = {}
    for key, value in dicts.items():
        if isinstance(value, dict):
            result[key] = dict_reduce(dicts[key], callback)
        else:
            result[key] = callback(dicts[key])
    return result


def _roboust_numpy_stat(values, callback):
    try:
        stat = callback(values, axis=0)
        if isinstance(stat, np.ndarray):
            return stat.tolist()
        else:
            return float(stat)
    except:
        return values


def dicts_numpy_stat(dicts: List[Dict], callback) -> Dict:
    dicts_sum = dict_concat(dicts)
    return dict_reduce(dicts_sum, lambda v: _roboust_numpy_stat(v, callback))
