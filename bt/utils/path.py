import os


def add_path_suffix(orignal_path: str, suffix: str) -> str:
    path, extension = os.path.splitext(orignal_path)
    return path + suffix + extension


def only_filename(path: str) -> str:
    directory, name = os.path.split(path)
    name, extension = os.path.splitext(name)
    return name