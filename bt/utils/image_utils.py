import SimpleITK as sitk
import os
import subprocess
import datetime
import threading


def show_image(image: sitk.Image):
    def thread_callback():
        file_path = "".join(f"/tmp/{datetime.datetime.now()}.mha".split(" "))
        sitk.WriteImage(sitk.Cast(image, sitk.sitkInt16), file_path)
        subprocess.call(["aliza", file_path])
        os.remove(file_path)

    thread = threading.Thread(target=thread_callback)
    thread.start()
