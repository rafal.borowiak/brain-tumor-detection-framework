import SimpleITK as sitk
from skimage.measure import label
import numpy


def clear_small_blobs(image: sitk.Image) -> sitk.Image:
    array = sitk.GetArrayFromImage(image > 0)
    labeled, number_of_labels = label(array, background=0, return_num=True)
    max_size = 0
    max_label = 0

    for i in range(1, number_of_labels + 1):
        current_sum = numpy.sum(labeled == i)
        if current_sum > max_size:
            max_size = current_sum
            max_label = i

    if max_label != 0:
        result = sitk.GetImageFromArray((labeled == max_label).astype(int))
        return sitk.Cast(
            sitk.Cast(result, image.GetPixelID()) * image,
            sitk.sitkInt16
        )
    else:
        return image
