import SimpleITK as sitk
from typing import List
from functional import seq
import functools


def binarize(image: sitk.Image, graylevels: List[int]):
    images = seq(graylevels) \
        .map(lambda graylevel: image == graylevel) \
        .to_list()

    images_sum = functools.reduce(lambda image1, image2: image1 | image2, images)

    return sitk.Cast(images_sum, sitk.sitkInt16)
