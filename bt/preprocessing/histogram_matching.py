from typing import List

import SimpleITK as sitk
from functional import seq


def match_histograms(inputs: List[sitk.Image], references: List[sitk.Image]) -> List[sitk.Image]:
    """
    Wykonuje algorytm dopasowania histogramu na przekazanych obrazach na podstawie obrazów odniesienia.
    Przed dopasowaniem obrazy są sprowadzane do 1024 poziomów jasności.

    :param inputs: lista obrazów wejściowych, których histogramy mają zostać dopasowane
    :param references: lista obrazów odniesienia, powinna mieć tyle elementów, co lista obrazów wejściowych
    :return: Zwraca listę obrazów po dopasowaniu histogramu.
    """
    return seq(list(zip(inputs, references))) \
        .map(lambda i: sitk.HistogramMatching(i[0], i[1], 1024)) \
        .to_list()
