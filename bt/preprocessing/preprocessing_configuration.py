import functools
from typing import List

from functional import seq
from jsonobject import *
import SimpleITK as sitk
import itertools
from fn import F
from fn.op import flip

from bt.filters import binarize


def _normalize(image: sitk.Image):
    return sitk.Normalize(image)


def _equalize(image: sitk.Image):
    return sitk.AdaptiveHistogramEqualization(image, alpha=0)


def _shrink(image: sitk.Image, shrink: int):
    return sitk.Shrink(image, list(itertools.repeat(shrink, 3)))


class PreprocessingConfiguration(JsonObject):
    shrink = IntegerProperty(default=1)
    normalize = BooleanProperty(default=False)
    equalize_histogram = BooleanProperty(default=False)
    binarized_graylevels = ListProperty(default=None)

    def is_neutral(self):
        return self.shrink == 1 and not self.normalize and not self.equalize_histogram

    def create_preprocessing_chain(self):
        predicate_functions = [
            (self.shrink != 1, F(flip(_shrink), self.shrink)),
            (self.binarized_graylevels, F(flip(binarize), self.binarized_graylevels)),
            (self.equalize_histogram, F(_equalize)),
            (self.normalize, F(_normalize))
        ]

        pipeline = F()

        for predicate, callback in predicate_functions:
            if predicate:
                pipeline >>= callback

        return pipeline
