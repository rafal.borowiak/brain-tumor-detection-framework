from typing import Dict, List

from bt.features import Classes
from sklearn import metrics


class ClassificationMetrics:
    """
    Klasa wyznaczająca miary jakości klasyfikacji.
    """
    def __init__(self, actual: Classes, predicted: Classes):
        """
        Konstruktor w którym wyznaczane są miary jakości klasyfikacji.

        :param actual: oczekiwany wynik klasifykacji
        :param predicted: przewidzany wynik klasyfikacji
        """
        assert actual.number_of_classes == predicted.number_of_classes
        actual_vector = actual.class_vector()
        predicted_vector = predicted.class_vector()

        self.precision, self.recall, self.dice, self.support = metrics.precision_recall_fscore_support(
            y_true=actual_vector,
            y_pred=predicted_vector,
            labels=actual.labels,
            average=None
        )

        self.precision = self.precision.tolist()[1:]
        self.recall = self.recall.tolist()[1:]
        self.dice = self.dice.tolist()[1:]
        self.support = self.support.tolist()
        cm = metrics.confusion_matrix(
            y_true=actual_vector,
            y_pred=predicted_vector,
            labels=actual.labels
        )
        self.confusion_matrix = cm.tolist()
        self.specificity = self._calculate_specificity(cm)[1:]

    def _calculate_specificity(self, cm):
        specifity = []
        for index in range(len(cm)):
            tp = cm[index][index]
            fp = cm[:, index].sum() - tp
            fn = cm[index].sum() - tp
            tn = cm.sum() - fn - fp - tp
            if tn + fp > 0:
                specifity += [float(tn / (tn + fp))]
            else:
                specifity += [1.0]
        return specifity

    def as_dict(self) -> Dict[str, List[float]]:
        """
        Konwertuje zapisuje miary klasyfikacji do słownika.

        :return: Zwraca słownik miar klasyfikacji
        """
        return {
            "precision": self.precision,
            "recall": self.recall,
            "dice": self.dice,
            "support": self.support,
            "confusion matrix": self.confusion_matrix,
            "specifity": self.specificity
        }
