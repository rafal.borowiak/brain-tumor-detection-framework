from typing import Dict

from bt.features import Classes
from bt.features.tissue import TumorRegion
from bt.metrics.classification_metrics import ClassificationMetrics
from bt.metrics.image_quality_measures import ImageQualityMeasures


class Metrics:
    """
    Klasa wyznaczająca miary jakości klasyfikacji dla każdego z obszarów guza i miary jakości obrazu.
    """
    def __init__(self, title: str, actual: Classes, predicted: Classes):
        """
        Konstruktor wyznaczający miary.

        :param title: dowolny ciąg znaków identyfikujący wyznaczone miary, np. nazwa pliku obrazu
        :param actual: oczekiwany wynik segmentacji
        :param predicted: rzeczywisty wynik segmentacji
        """
        self.title = title

        self.general_score = ClassificationMetrics(actual, predicted)

        self.core_score = ClassificationMetrics(
            actual.map_as_positive(TumorRegion.Core),
            predicted.map_as_positive(TumorRegion.Core)
        )

        self.active_score = ClassificationMetrics(
            actual.map_as_positive(TumorRegion.Active),
            predicted.map_as_positive(TumorRegion.Active)
        )

        self.whole_score = ClassificationMetrics(
            actual.map_as_positive(TumorRegion.Whole),
            predicted.map_as_positive(TumorRegion.Whole)
        )

        self.image_quality_measures = ImageQualityMeasures(
            actual.image,
            predicted.image
        )

    def as_dict(self) -> Dict:
        """
        Konwertuje miary do postaci słownika.

        :return: Słownik z miarami dla wszystkich obszarów.
        """
        return {
            "title": self.title,
            "all classes": self.general_score.as_dict(),
            "tumor core": self.core_score.as_dict(),
            "whole tumor": self.whole_score.as_dict(),
            "active tumor": self.active_score.as_dict(),
            "image quality measures": self.image_quality_measures.as_dict()
        }
