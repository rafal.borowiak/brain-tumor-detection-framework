from typing import Dict

import numpy
import SimpleITK as sitk
import skimage.measure
import sklearn
from lazy import lazy


class ImageQualityMeasures:
    """
    Klasa wyznaczająca miary jakości obrazu.
    """
    def __init__(self, actual: sitk.Image, predicted: sitk.Image):
        """
        Konstruktor inicjalizujący pola klasy.

        :param actual: obraz oczekiwany
        :param predicted: obraz uzyskany w wyniku segmentacji
        """
        self._actual = sitk.GetArrayFromImage(actual).astype(int)
        self._predicted = sitk.GetArrayFromImage(predicted).astype(int)

    @lazy
    def error(self):
        return self._actual - self._predicted

    @lazy
    def mean_squared_error(self):
        return numpy.mean(self.error * self.error)

    @lazy
    def peak_signal_to_noise_ratio(self):
        return 10 * numpy.log(self._actual.max() ** 2 / self.mean_squared_error) / numpy.log(10)

    @lazy
    def normalized_cross_correlation(self):
        return numpy.sum(self._predicted * self._actual) / numpy.sum(self._actual * self._actual)

    @lazy
    def structural_content(self):
        return numpy.sum(self._actual * self._actual) / numpy.sum(self._predicted * self._predicted)

    @lazy
    def maximum_difference(self):
        return numpy.max(self.error)

    @lazy
    def normalized_absolute_error(self):
        return numpy.sum(numpy.abs(self.error)) / numpy.sum(self._actual)

    @lazy
    def structural_similarity(self):
        return skimage.measure.compare_ssim(self._actual, self._predicted)

    @lazy
    def universal_image_quality_index(self):
        actual_mean = self._actual.mean()
        predicted_mean = self._predicted.mean()

        actual_variance = numpy.var(self._actual, ddof=1)
        predicted_variance = numpy.var(self._predicted, ddof=1)

        product_variance = numpy.multiply(self._actual - actual_mean, self._predicted - predicted_mean).sum() \
                           / (numpy.product(self._actual.shape) - 1)

        return (4 * product_variance * actual_mean * predicted_mean) \
                / ((actual_variance + predicted_variance) * (actual_mean ** 2 + predicted_mean ** 2))

    def as_dict(self) -> Dict[str, float]:
        """
        :return: Zwraca słownik wyznaczonych miar jakości obrazu.
        """
        return {
            "mean squared error": float(self.mean_squared_error),
            "peak signal to noise ratio": float(self.peak_signal_to_noise_ratio),
            "normalized cross correlation": float(self.normalized_cross_correlation),
            "normalized absolute error": float(self.normalized_absolute_error),
            "structural content": float(self.structural_content),
            "maximum difference": float(self.maximum_difference),
            "structural similarity": float(self.structural_similarity),
            "universal image quality index": float(self.universal_image_quality_index)
        }
